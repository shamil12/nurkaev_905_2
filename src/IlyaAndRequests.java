/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Task C
 */

import java.util.Scanner;

public class IlyaAndRequests {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        int m = scanner.nextInt();
        int[] a = new int[s.length()];
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1))
                a[i] += a[i - 1] + 1;
            else a[i] = a[i - 1];
        }
        for (int i = 0; i < m; i++) {
            int l = scanner.nextInt();
            int r = scanner.nextInt();
            System.out.print(a[r - 1] - a[l - 1] + "\n");
        }
    }
}
