package com.c3.common;

/*
 * ROCKET SCIENCE
 *
 * Добиться работы программы. Добиться корректной работы программы. Добиться эффективной работы программы
 *
 * 1. Написать интерфейс Fuel, который реализуется, например, классом UDMH. Интерфейс должен:
 *    а) содержать методы, возвращающие количество атомов углерода, водорода, азота и кислорода в формуле топливе;
 *    б) быть унаследованным от интерфейса IMatter;
 *    в) находиться в пакете com.c3.fuels.
 * 2. Написать интерфейс Oxidizer, который реализуется, например, классом NitrogenTetroxide. Интерфейс должен:
 *    а) содержать методы, возвращающие количество атомов азота и кислорода в формуле окислителя;
 *    б) быть унаследованным от интерфейса IMatter;
 *    в) находиться в пакете com.c3.oxidizers.
 * 3. Написать классы Kerosene и Hydrogen. Классы должны:
 *    а) реализовывать интерфейс Fuel;
 *    б) находиться в пакете com.c3.fuels;
 *    в) формула керосина C12H26;
 *    г) формула водорода H2.
 * 4. Написать класс LiquidOxygen. Класс должен:
 *    а) реализовывать интерфейс Oxidizer;
 *    б) находиться в пакете com.c3.oxidizers;
 *    в) формула жидкого кислорода (как и обычного) O2.
 * 5. Написать исключение InsufficientMatterException.
 *    Исключение выбрасывается при попытке уменьшения массы вещества до отрицательной величины
 *    У исключения есть параметр left, содержащий оставшуюся массу вещества
 * 6. Написать класс бака Tank<T>. Класс T - содержимое бака - должен реализовывать IMatter. Класс Tank<T> должен:
 *    а) обладать конструктором с параметром T - начальным содержимым бака
 *    б) иметь метод next, принимающий параметр mass и возвращающий T - оставшееся в баке вещество после уменьшения
 *       массы на mass; метод next может выбрасывать исключение InsufficientMatterException, если массы недостаточно
 *    в) иметь метод remainingMass, возвращающий массу оставшегося в баке вещества
 * 7. Написать исключение UnstableEngineException.
 *    Исключение выбрасывается если в баках недостаточно окислителя или топлива
 *    У исключения есть параметры fuelLeft и oxidizerLeft, содержащие оставшееся количество окислителя и топлива
 *    Можно создать исключение с текстовым описанием. В этом случае параметры выше устанавливаются в нуль
 * 8. Написать компаратор RocketComparator ракет (Rocket).
 *    Компаратор суммирует результаты вызова метода burn() до выбрасывания исключения. Та ракета лучше, у которой
 *    этот результат больше.
 *
 * В результате дописывания кода программа должна:
 * а) компилироваться
 * б) выдавать только ok на каждом из тестов (не должно быть FAIL или исключений до надписи FUN)
 * в) по желанию: выдавать максимум в TOTAL; выдавать физически осмысленные значения
 *
 */

import com.c3.exceptions.InsufficientMatterException;
import com.c3.exceptions.UnstableEngineException;
import com.c3.fuels.Fuel;
import com.c3.fuels.Hydrogen;
import com.c3.fuels.Kerosene;
import com.c3.fuels.UDMH;
import com.c3.oxidizers.LiquidOxygen;
import com.c3.oxidizers.NitrogenTetroxide;
import com.c3.oxidizers.Oxidizer;
import com.c3.test.TestMatter;
import com.c3.test.TestRocket;

public class Main {

    public static void main(String[] args) {
        System.out.println("*** TESTS ***");

        System.out.print("Fuel interface existence... ");
        Fuel f1 = new UDMH(0);
        System.out.println("ok");

        System.out.print("Oxidizer interface existence... ");
        Oxidizer o1 = new NitrogenTetroxide(0);
        System.out.println("ok");

        //////// Test kerosene

        System.out.print("Kerosene class existence... ");
        Fuel f2 = new Kerosene(10);
        System.out.println("ok");

        testFuel("Kerosene", f2, 12, 26, 0, 0);

        //////// Test hydrogen

        System.out.print("Hydrogen class existence... ");
        Fuel f3 = new Hydrogen(10);
        System.out.println("ok");

        testFuel("Hydrogen", f3, 0, 2, 0, 0);

        System.out.print("LOX class existence... ");
        Oxidizer o2 = new LiquidOxygen(10);
        System.out.println("ok");

        testOxidizer("Oxygen", o2, 0, 2);

        //////// Test Tank<T>

        TestMatter tm = new TestMatter(10);

        System.out.print("Tank<T> existence... ");
        Tank<TestMatter> testTank = new Tank<>(tm);
        System.out.println("ok");

        test("Tank remaining mass equals to 10", testTank.remainingMass(), 10);

        System.out.print("Tank can give 1 mass... ");
        try {
            testTank.next(1);
            System.out.println("ok");
        } catch (InsufficientMatterException e) {
            System.out.println("FAIL");
        }

        test("Tank now has 9 mass", testTank.remainingMass(), 9);

        System.out.print("Tank cannot give 10 mass... ");
        try {
            testTank.next(10);
            System.out.println("FAIL");
        } catch (InsufficientMatterException e) {
            System.out.println("ok");

            test("InsufficientMatterException has correct left mass", e.getLeft(), 9);
        }

        System.out.print("Tank can give 9 mass... ");
        try {
            testTank.next(9);
            System.out.println("ok");
        } catch (InsufficientMatterException e) {
            System.out.println("FAIL");
        }

        // Test comparator

        testCmp("Exploding rockets are equal", new TestRocket(0, 100), new TestRocket(0, 50), 0);
        testCmp("But flying rocket is better", new TestRocket(1, 1), new TestRocket(0, 100), 1);
        testCmp("Flying rocket is better on the other hand too", new TestRocket(0, 100), new TestRocket(1, 1), -1);
        testCmp("Some rockets are equal", new TestRocket(4, 3), new TestRocket(2, 6), 0);
        testCmp("On any hand", new TestRocket(2, 6), new TestRocket(4, 3), 0);
        testCmp("Some are not", new TestRocket(2, 6), new TestRocket(2, 5), 1);
        testCmp("Fast sometimes is worse than slow", new TestRocket(1, 11), new TestRocket(4, 3), -1);
        testCmp("Sometimes fast is better than slow", new TestRocket(1, 11), new TestRocket(5, 2), 1);

        System.out.println();
        System.out.println("**** FUN ****");
        Tank<UDMH> udmhTank = new Tank<>(new UDMH(100));
        Tank<NitrogenTetroxide> n2o4Tank = new Tank<>(new NitrogenTetroxide(100));
        Rocket rocket = new Rocket();
        rocket.connectFuelTank(udmhTank);
        rocket.connectOxidizerTank(n2o4Tank);
        rocket.setFuelPart(9);
        rocket.setOxidizerPart(9);

        int s = 0;
        try {
            while (true) {
                int x = rocket.burn();
                System.out.println(x);
                s += x;
            }
        } catch (UnstableEngineException e) {
            System.out.println("TOTAL: " + s);
            e.printStackTrace();
        }
    }

    private static void testCmp(String name, Rocket r1, Rocket r2, int expected) {
        System.out.print(name + "... ");
        int actual = new RocketComparator().compare(r1, r2);
        int sgnA = actual > 0 ? 1 : actual < 0 ? -1 : 0;
        if (sgnA == expected)
            System.out.println("ok");
        else
            System.out.println("FAIL");
    }

    private static void testOxidizer(String name, Oxidizer oxidizer, int n, int o) {
        test(name + " has " + n + " nitrogen atoms", oxidizer.getNitrogenAtomsCount(), n);
        test(name + " has " + o + " oxygen atoms", oxidizer.getOxygenAtomsCount(), o);
        testMatter(name, oxidizer);
    }

    private static void testFuel(String name, Fuel fuel, int c, int h, int n, int o) {
        test(name + " has " + c + " carbon atoms", fuel.getCarbonAtomsCount(), c);
        test(name + " has " + h + " hydrogen atoms", fuel.getHydrogenAtomsCount(), h);
        test(name + " has " + n + " nitrogen atoms", fuel.getNitrogenAtomsCount(), n);
        test(name + " has " + o + " oxygen atoms", fuel.getOxygenAtomsCount(), o);
        testMatter(name, fuel);
    }

    private static void testMatter(String name, IMatter matter) {
        test(name + " has mass 10", matter.getMass(), 10);

        System.out.print(name + " mass can be decreased by 1... ");
        try {
            matter.decreaseMass(1);
            System.out.println("ok");
        } catch (InsufficientMatterException e) {
            System.out.println("FAIL");
        }

        test(name + " has mass 9", matter.getMass(), 9);

        System.out.print(name + " mass cannot be decreased by 10... ");
        try {
            matter.decreaseMass(10);
            System.out.println("FAIL");
        } catch (InsufficientMatterException e) {
            System.out.println("ok");

            test("InsufficientMatterException has correct left mass", e.getLeft(), 9);
        }

        System.out.print(name + " mass can be decreased by 9... ");
        try {
            matter.decreaseMass(9);
            System.out.println("ok");
        } catch (InsufficientMatterException e) {
            System.out.println("FAIL");
        }
    }

    private static <T> void test(String name, T actual, T expected) {
        System.out.print(name + "... ");
        if (expected == null && actual == null || expected.equals(actual))
            System.out.println("ok");
        else
            System.out.println("FAIL");
    }
}
