/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package com.c3.common;

import com.c3.exceptions.UnstableEngineException;

import java.util.Comparator;

public class RocketComparator<T extends Rocket> implements Comparator<T> {

    @Override
    public int compare(Rocket r1, Rocket r2) {
        return countBurn(r1) - countBurn(r2);
    }

    private int countBurn(Rocket r) {
        int result = 0;

        try {
            while (true) {
                result += r.burn();
            }
        } catch (UnstableEngineException e) {
            return result;
        }
    }
}
