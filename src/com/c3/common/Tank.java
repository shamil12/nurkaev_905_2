/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package com.c3.common;

import com.c3.exceptions.InsufficientMatterException;

public class Tank<T extends IMatter> {

    private T mass;

    public Tank(T mass) {
        this.mass = mass;
    }

    public T next(int mass) throws InsufficientMatterException {
        this.mass.decreaseMass(mass);
        return this.mass;
    }

    public int remainingMass() {
        return mass.getMass();
    }
}
