/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package com.c3.exceptions;

public class InsufficientMatterException extends Throwable {
    private static int left;

    public InsufficientMatterException(int l) {
        super("left: " + left);
        left = l;
    }

    public int getLeft() {
        return left;
    }
}
