/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package com.c3.exceptions;

public class UnstableEngineException extends Exception {
    private static int fuelLeft;
    private static int oxidizerLeft;

    public UnstableEngineException() {
    }

    public UnstableEngineException(String left) {
        super("left: " + left);
        fuelLeft = 0;
        oxidizerLeft = 0;
    }

    public UnstableEngineException(int i, int left) {
        super("Not enough oxidizer/fuel");
        fuelLeft = 0;
        oxidizerLeft = 0;
    }
}
