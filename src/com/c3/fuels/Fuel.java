package com.c3.fuels;

import com.c3.common.IMatter;
import com.c3.oxidizers.Oxidizer;

public interface Fuel extends IMatter, Oxidizer {
    public int getCarbonAtomsCount();

    public int getHydrogenAtomsCount();

    public int getNitrogenAtomsCount();

    public int getOxygenAtomsCount();
}
