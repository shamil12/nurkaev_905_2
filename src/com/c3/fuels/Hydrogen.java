/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package com.c3.fuels;

import com.c3.exceptions.InsufficientMatterException;

public class Hydrogen implements Fuel{

    private int mass;

    public Hydrogen(int mass) {
        this.mass = mass;
    }

    @Override
    public int getCarbonAtomsCount() {
        return 0;
    }

    @Override
    public int getHydrogenAtomsCount() {
        return 2;
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 0;
    }

    @Override
    public int getMass() {
        return mass;
    }

    public void decreaseMass(int mass) throws InsufficientMatterException {
        if (this.mass < mass)
            throw new InsufficientMatterException(this.mass);
        this.mass -= mass;
    }
}
