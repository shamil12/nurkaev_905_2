/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package com.c3.oxidizers;

import com.c3.exceptions.InsufficientMatterException;

public class LiquidOxygen implements Oxidizer {

    private int mass;

    public LiquidOxygen(int mass) {
        this.mass = mass;
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 2;
    }

    @Override
    public int getMass() {
        return mass;
    }

    public void decreaseMass(int mass) throws InsufficientMatterException {
        if (this.mass < mass)
            throw new InsufficientMatterException(this.mass);
        this.mass -= mass;
    }
}
