package com.c3.oxidizers;

import com.c3.common.IMatter;

public interface Oxidizer extends IMatter {
    public int getNitrogenAtomsCount();

    public int getOxygenAtomsCount();
}
