/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * My algorithms
 */

package my.algorithms.sorts;

public class Array_sort {

    private static int[] insertion_sort(int[] a) {
        int N = a.length, i, j, t;
        for (i = 0; i < N - 1; i++) {
            for (j = i + 1; j > 0; j--) {
                if (a[j - 1] <= a[j]) break;
                else {
                    t = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = t;
                }
            }
        }
        return a;
    }

    private static int[] selection_sort(int[] a) {
        int N = a.length, i, j, t;
        for (i = 0; i < N - 1; i++) {
            for (j = i + 1; j < N; j++) {
                if (a[j] < a[i]) {
                    t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
        }
        return a;
    }

    private static int[] bubble_sort(int[] a) {
        int N = a.length, i, j, t;
        for (i = 0; i < N - 1; i++) {
            for (j = 1; j < N - i; j++) {
                if (a[j - 1] > a[j]) {
                    t = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = t;
                }
            }
        }
        return a;
    }

    private static int[] shaker_sort(int[] a) {
        int N = a.length, i, j, k, t;
        for (i = 0; i < N / 2; i++) {
            for (j = 1; j < N - i; j++) {
                if (a[j - 1] > a[j]) {
                    t = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = t;
                }
            }
            for (k = j - 1; k > i; k--) {
                if (a[k - 1] > a[k]) {
                    t = a[k - 1];
                    a[k - 1] = a[k];
                    a[k] = t;
                }
            }
        }
        return a;
    }

    private static int[] oddEven_sort(int[] a) {
        int N = a.length, i, j, t;
        for (i = 0; i < N / 2; i++) {
            for (j = 1; j < N; j += 2) {
                if (a[j - 1] > a[j]) {
                    t = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = t;
                }
            }
            for (j = 2; j < N; j += 2) {
                if (a[j - 1] > a[j]) {
                    t = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = t;
                }
            }
        }
        return a;
    }

    private static int[] comb_sort(int[] a) {
        int N = a.length, i, t, gap = N;
        while (gap > 0) {
            for (i = 0; i < N - gap; i++) {
                if (a[i] > a[i + gap]) {
                    t = a[i];
                    a[i] = a[i + gap];
                    a[i + gap] = t;
                }
            }
            gap /= 1.25;
        }
        return a;
    }

    public static void main(String[] args) {
//        System.out.println(Arrays.toString(insertion_sort(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})));
//        System.out.println(Arrays.toString(selection_sort(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})));
//        System.out.println(Arrays.toString(bubble_sort(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})));
//        System.out.println(Arrays.toString(shaker_sort(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})));
//        System.out.println(Arrays.toString(oddEven_sort(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})));
//        System.out.println(Arrays.toString(comb_sort(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0})));
    }
}
