/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task H
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.math.BigInteger;
import java.util.Scanner;

public class Corridor {

    static int tileOne;
    static int tileThree;
    static BigInteger count = BigInteger.valueOf(0);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a = sc.nextInt();
        int b = sc.nextInt();

        if (a + 3 * b < n || n == 0) {
            System.out.println(0);
        } else if ((n == 1 && a > 0) || (n == 2 && a > 1)) {
            System.out.println(1);
        } else {
            int lengthCovering = 0;

            for (int i = 0; i < b && lengthCovering < n; i++) {
                lengthCovering += 3;
                tileThree += 1;
            }
            if (lengthCovering > n) {
                lengthCovering -= 3;
                tileThree -= 1;
            }
            for (int i = 0; i < a && lengthCovering < n; i++) {
                lengthCovering += 1;
                tileOne += 1;
            }
            if (lengthCovering > n) {
                lengthCovering -= 1;
                tileOne -= 1;
            }
            if (lengthCovering == n) {
                count = calculateNumberPermutRepet();
            }

            while (tileOne <= a - 3) {
                tileThree -= 1;
                tileOne += 3;
                lengthCovering = tileOne + 3 * tileThree;
                if (lengthCovering == n) {
                    count = count.add(calculateNumberPermutRepet());
                }
            }

            System.out.println(count.mod(BigInteger.valueOf(1000000009)));
        }
    }

    private static BigInteger calculateNumberPermutRepet() {
        BigInteger a = BigInteger.valueOf(1);
        BigInteger b = BigInteger.valueOf(1);
        BigInteger c = BigInteger.valueOf(1);

        for (int i = 1; i <= tileOne; i++) {
            a = a.multiply(BigInteger.valueOf(i));
        }

        for (int i = 1; i <= tileThree; i++) {
            b = b.multiply(BigInteger.valueOf(i));
        }

        for (int i = 1; i <= tileOne + tileThree; i++) {
            c = c.multiply(BigInteger.valueOf(i));
        }

        return c.divide(a.multiply(b));
    }
}