/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task A
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.Collections;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class ExchangeRequests {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int s = sc.nextInt();

        TreeMap<Integer, Integer> Buy = new TreeMap<>(Collections.reverseOrder());
        TreeMap<Integer, Integer> Sell = new TreeMap<>();

        String d;
        int p;
        int q;
        for (int i = 0; i < n; i++) {
            d = sc.next();
            p = sc.nextInt();
            q = sc.nextInt();
            if (d.equals("S")) {
                Sell.putIfAbsent(p, 0);
                Sell.put(p, q + Sell.get(p));
            } else {
                Buy.putIfAbsent(p, 0);
                Buy.put(p, q + Buy.get(p));
            }
        }

        int i = 0;
        if (!Sell.isEmpty()) {
            int[] a, b;
            if (Sell.size() < s) {
                a = new int[Sell.size()];
                b = new int[Sell.size()];
                for (Map.Entry<Integer, Integer> entry : Sell.entrySet()) {
                    a[i] = entry.getKey();
                    b[i] = entry.getValue();
                    i++;
                }
                for (i = Sell.size() - 1; i >= 0; i--) {
                    System.out.println("S" + " " + a[i] + " " + b[i]);
                }
            } else {
                a = new int[s];
                b = new int[s];
                for (Map.Entry<Integer, Integer> entry : Sell.entrySet()) {
                    if (i >= s) break;
                    a[i] = entry.getKey();
                    b[i] = entry.getValue();
                    i++;
                }
                for (i = s - 1; i >= 0; i--) {
                    System.out.println("S" + " " + a[i] + " " + b[i]);
                }
            }
        }

        i = 0;
        for (Map.Entry<Integer, Integer> entry : Buy.entrySet()) {
            if (i >= s) break;
            System.out.println("B" + " " + entry.getKey() + " " + entry.getValue());
            i++;
        }
    }
}