/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task G
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.Scanner;

public class Fence {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int[] h = new int[n];
        h[0] = sc.nextInt();
        for (int i = 1; i < n; i++) {
            h[i] += sc.nextInt() + h[i - 1];
        }

        int minSum = h[k - 1];
        int pos = 1;

        for (int i = k; i < n; i++) {
            if (h[i] - h[i - k] < minSum) {
                minSum = h[i] - h[i - k];
                pos = i - k + 2;
            }
        }

        System.out.println(pos);
    }
}
