/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task E
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.Scanner;

public class OnesZero {

    static Scanner sc = new Scanner(System.in);
    static final int n = sc.nextInt();
    static final int k = sc.nextInt();
    static int[] y = new int[n];
    static int count = 0;

    public static void main(String[] args) {
        putOne(0, 0);

        System.out.println(count);
    }

    private static void putOne(int x, int countForOnePerm) {
        if (countForOnePerm == k) {
            count += 1;
        } else if (x < n) { // координата X
            for (y[x] = 0; y[x] < n; y[x]++) { // перебор координат y(x)
                if (correct(x)) putOne(x + 1, countForOnePerm + 1);
            }
            y[x] *= 100;
            putOne(x + 1, countForOnePerm);
        }
    }

    private static boolean correct(int x) {
        for (int x1 = 0; x1 < x; x1++)
            if (y[x1] == y[x] || Math.abs(x - x1) == Math.abs(y[x] - y[x1]))
                return false;
        return true;
    }
}
