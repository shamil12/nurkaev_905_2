/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task C
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.Scanner;

public class QueueShower {

    static int[] a = {0, 1, 2, 3, 4};
    static int N = a.length;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[][] g = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j >= i) {
                    g[i][j] = sc.nextInt();
                } else {
                    g[i][j] = sc.nextInt() + g[j][i];
                    g[j][i] = g[i][j];
                }
            }
        }

        int maxJoy = 0;
        int s;
        for (int i = 0; i < 119; i++) {
            s = g[a[0]][a[1]] + g[a[1]][a[2]] + 2 * (g[a[2]][a[3]] + g[a[3]][a[4]]);
            if (s > maxJoy) {
                maxJoy = s;
            }
            oneTransposition();
        }

        s = g[a[0]][a[1]] + g[a[1]][a[2]] + 2 * (g[a[2]][a[3]] + g[a[3]][a[4]]);
        if (s > maxJoy) {
            maxJoy = s;
        }

        System.out.println(maxJoy);
    }

    private static void oneTransposition() {
        int i;
        for (i = N - 2; a[i] > a[i + 1]; i--) ;
        reverseRightPart(i + 1);
        finish(i);
    }

    private static void finish(int i) {
        int k = i + 1;
        while (a[i] > a[k]) k++;
        swap(i, k);
    }

    private static void reverseRightPart(int pos) {
        int lengthPart = N - pos, k = pos;
        for (int i = 0; i < lengthPart / 2; i++) {
            swap(k, N - i - 1);
            k++;
        }
    }

    private static void swap(int i, int j) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
