/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task B
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Reposts {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Map<String, Integer> map = new HashMap<>();

        String name1;
        String name2;
        int maxChain = 1;
        for (int i = 0; i < n; i++) {
            name1 = sc.next().toLowerCase();
            sc.next();
            name2 = sc.next().toLowerCase();

            map.putIfAbsent(name2, 1);
            map.putIfAbsent(name1, 1);

            map.put(name1, map.get(name2) + 1);

            if (map.get(name1) > maxChain) {
                maxChain = map.get(name1);
            }
        }

        System.out.println(maxChain);
    }
}
