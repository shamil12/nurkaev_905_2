/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task D
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.Arrays;
import java.util.Scanner;

public class SecurityGuard {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[][] Rooms = new int[n][m + 2];
        for (int i = 0; i < n; i++) {
            Rooms[i] = Arrays.stream(sc.next().split(""))
                    .mapToInt(Integer::parseInt).toArray();
        }

        int[][] a = new int[n][2];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m + 2; j++) {
                if (Rooms[i][j] == 1) {
                    a[i][1] = m + 1 - j;
                    break;
                }
            }

            for (int j = m + 1; j >= 0; j--) {
                if (Rooms[i][j] == 1) {
                    a[i][0] = j;
                    break;
                }
            }
        }

        int l = a[0][0];
        int r = a[0][1];
        int l2;

        for (int i = 1; i < n; i++) {
            if (l == 0) l2 = a[i][0];
            else l2 = Math.min(2 * a[i][0] + 1 + l, m + 2 + r);

            if (r == 0) r = a[i][1];
            else r = Math.min(2 * a[i][1] + 1 + r, m + 2 + l);
            l = l2;
        }
        System.out.println(l);
    }

}
