/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Contest 2
 * Task F
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest2;

import java.util.Scanner;

public class Sesurity {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] A = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                A[i][j] = sc.nextInt();
            }
        }

        int[][] F = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                F[i][j] = A[i][j] + Math.max(i - 1 >= 0 ? F[i - 1][j] : 0,
                        j - 1 >= 0 ? F[i][j - 1] : 0);
            }
        }

        System.out.println(F[n - 1][n - 1]);
    }
}
