/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Task A
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest3;

import java.util.Scanner;

public class Hydrometcentre {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int maxlen = 0, currlen = 0;
        for (int i = 0; i < N; i++) {
            int x = scanner.nextInt();
            if (x > -2 || (x >> 1) << 1 != x) {
                currlen = 0;
                continue;
            }
            if (++currlen > maxlen)
                maxlen = currlen;
        }
        System.out.print(maxlen);
    }
}
