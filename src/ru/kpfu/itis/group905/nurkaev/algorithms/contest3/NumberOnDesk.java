/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Task D
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest3;

import java.util.Arrays;
import java.util.Scanner;

public class NumberOnDesk {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        char[] digits = scanner.next().toCharArray();
        Arrays.sort(digits);
        int sum = 0;
        for (char c : digits)
            sum += c - '0';
        int i = 0; // minCount
        while (sum < k) {
            sum += 9 - digits[i] + '0'; // на сколько надо увеличить цифру, чтобы получилось 9
            i++;
        }
        System.out.print(i);
    }
}