/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Task E
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.contest3;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Permutation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < n; i++) {
            set.add(scanner.nextInt());
        }
        int minCount = 0;
        for (int i = 1; i <= n; i++)
            if (!set.contains(i))
                minCount++;
        System.out.print(minCount);
    }
}