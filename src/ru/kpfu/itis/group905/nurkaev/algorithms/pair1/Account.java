/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair1;

public class Account<T extends Number> implements Accountable{
    private T id;
    private T sum;

    Account(T id, T sum) {
        this.id = id;
        this.sum = sum;
    }

    public T getId() {
        return id;
    }

    public T getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = (T) ((Number) sum);
    }

    public void addToSum(T sum) {
        this.sum = (T) ((Number) (this.sum.doubleValue() + sum.doubleValue()));
    }

    public void subtractToSum(T sum) {
        this.sum = (T) ((Number) (this.sum.doubleValue() - sum.doubleValue()));
    }

    public String toString() {
        return "Id: " + getId() + ", Sum: " + getSum();
    }

    public static void main(String[] args) {
        Account<Double> a = new Account<>(111.111, 120.1);
        a.addToSum(9.9);
        System.out.println(a);
    }
}
