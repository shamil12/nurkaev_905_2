/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair1;

public interface Accountable<T> {
    T getId();

    T getSum();

    void setSum(int sum);
}
