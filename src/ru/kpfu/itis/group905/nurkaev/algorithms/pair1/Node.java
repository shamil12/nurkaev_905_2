/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair1;

public class Node {
    public int data;
    public Node next;
    public Node prev;
}