/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair1;

import java.util.Arrays;

public class SortingAlgorithms {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 6, 7};
        int[] b = {3, 4, 5, 6, 8, 9, 10};
//        System.out.println(Arrays.toString(diff(a, b)));
//        System.out.println(Arrays.toString(merge(a, b)));
        System.out.println(Arrays.toString(intersection(a, b)));
    }

    public static int[] merge(int[] a, int[] b) {
        int n = a.length + b.length;
        int[] c = new int[n];

        int i = 0, j = 0, k;
        for (k = 0; k < n && i < a.length && j < b.length; k++) {
            if (a[i] <= b[j]) {
                c[k] = a[i];
                i++;
            } else {
                c[k] = b[j];
                j++;
            }
        }

        while (i < a.length) {
            c[k] = a[i];
            k++;
            i++;
        }
        while (j < b.length) {
            c[k] = b[j];
            k++;
            j++;
        }
        return c;
    }

    public static int[] diff(int[] a, int[] b) {
        int n = Integer.min(a.length, b.length);
        int[] c = new int[n];

        int i = 0, j = 0, k;
        for (k = 0; k < n && i < a.length && j < b.length; k++) {
            if (a[i] < b[j]) {
                c[k] = a[i];
                i++;
            } else if (b[j] < a[i]) {
                c[k] = b[j];
                j++;
            } else {
                i++;
                j++;
                k--;
            }
        }

        while (i < a.length) {
            c[k] = a[i];
            k++;
            i++;
        }
        while (j < b.length) {
            c[k] = b[j];
            k++;
            j++;
        }
        return removeZero(c);
    }

    public static int[] intersection(int[] a, int[] b) {
        int n = Integer.min(a.length, b.length);
        int[] c = new int[n];

        int i = 0, j = 0, k;
        for (k = 0; k < n && i < a.length && j < b.length; k++) {
            if (a[i] < b[j]) {
                i++;
                k--;
            } else if (b[j] < a[i]) {
                j++;
                k--;
            } else {
                c[k] = a[i];
                i++;
                j++;
            }
        }
        return removeZero(c);
    }

    private static int[] removeZero(int[] a) {
        int i = 0;
        while (a[i] != 0) {
            i++;
        }
        return Arrays.copyOf(a, i);
    }
}