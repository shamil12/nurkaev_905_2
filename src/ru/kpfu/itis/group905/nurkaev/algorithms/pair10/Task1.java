/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair10;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        int[] a = {3, 1, 2, 1, 6, 1, 3, 6, 2};
        System.out.println(Arrays.toString(nearestSmallerLeft(a)));
    }

    private static int[] nearestSmallerLeft(int[] a) {
        int[] res = new int[a.length];

        for (int i = a.length - 1; i >= 0; i--) {
            if (a[i] <= 1) res[i] = -1;
            else {
                int posRes = i;
                while (posRes >= 0 && a[posRes] >= a[i]) posRes--;
                res[i] = posRes >= 0 ? a[posRes] : -1;
            }
        }

        return res;
    }
}
