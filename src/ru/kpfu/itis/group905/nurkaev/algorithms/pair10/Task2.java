/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair10;

import java.util.Arrays;
import java.util.LinkedList;

public class Task2 {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>(Arrays.asList(1, 1, 1, 2, 3));
        System.out.println(removeDuplicates(list));
    }

    private static LinkedList<Integer> removeDuplicates(LinkedList<Integer> list) {
        for (int i = 1; i < list.size(); i++) {
            int val = list.get(i - 1);
            while (i < list.size() && list.get(i) == val) {
                list.remove(i);
            }
        }
        return list;
    }
}
