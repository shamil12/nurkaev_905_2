/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair10;

public class Task3 {
    public static void main(String[] args) {
        Node head = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        head.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = null;
        int size = 4; // количество всех node
        System.out.println(cycle(head, size));
    }

    // возвращаем значение (подразумевается, что все они различны)
    private static int cycle(Node head, int size) {
        Node curr = head;
        while (curr.next != null) {
            Node temp = curr;
            curr = curr.next;
            temp.next = null;
            size--;
        }
        if (size == 1) return 0;
        return curr.val;
    }

    static class Node {
        public int val;
        public Node next;

        public Node(int val) {
            this.val = val;
        }
    }
}