/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * For LargeNumber
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair2;

public class Comparator implements java.util.Comparator<Integer> {

    @Override
    public int compare(Integer a, Integer b) {
        int x = Integer.parseInt("" + a + b);
        int y = Integer.parseInt("" + b + a);
        if (x > y) return -1;
        else if (x < y) return 1;
        return 0;
    }
}
