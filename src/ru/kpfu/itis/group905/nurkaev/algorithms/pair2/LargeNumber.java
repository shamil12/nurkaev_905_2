/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class LargeNumber {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(3, 30, 5, 34, 9));
        Collections.sort(a, new Comparator());

        String s = "";
        for (int e : a) {
            s += e;
        }

        System.out.println(s);
    }
}
