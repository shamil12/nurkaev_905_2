/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair2;

import java.util.Arrays;

public class MergeMxM {
    static int s = 0;
    static int[] m = {5, 6, 3, 2};
    static int[][] a = {
            {1, 2, 6, 13, 23},
            {1, 6, 6, 7, 9, 10},
            {8, 12, 13},
            {9, 10}
    };
    static int[] b = new int[sum(m)];

    public static void main(String[] args) {
        System.out.println(Arrays.toString(heapSort(a, m)));
    }

    public static int[] heapSort(int[][] a, int[] m) {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i]; j++) {
                add(a[i][j]);
            }
        }
        int[] sortedArray = new int[b.length];
        for (int i = 0; i < b.length; i++) {
            sortedArray[i] = minDel();
        }
        return sortedArray;
    }

    private static void add(int x) {
        b[s] = x;
        s++;
        int i = s - 1;
        while (i != 0 && b[i] < b[(i - 1) / 2]) {
            swap(i, (i - 1) / 2);
            i = (i - 1) / 2;
        }
    }

    private static int minDel() {
        int ans = b[0];
        swap(0, s - 1);
        s--;
        int i = 0, m = 2 * i + 1;
        if (m + 1 < s && b[m] > b[m + 1]) m++;
        while (m < s && s > 1 && b[i] > b[m]) {
            swap(i, m);
            i = m;
            m = 2 * i + 1;
            if (m + 1 < s && b[m] > b[m + 1]) m++;
        }
        return ans;
    }

    private static void swap(int i, int j) {
        int temp = b[i];
        b[i] = b[j];
        b[j] = temp;
    }

    private static int sum(int[] a) {
        int sum = 0;
        for (int i : a) {
            sum += i;
        }
        return sum;
    }
}
