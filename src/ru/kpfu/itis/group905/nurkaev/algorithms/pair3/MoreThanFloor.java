/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 2
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair3;

import java.util.HashMap;

public class MoreThanFloor {
    public static void main(String[] args) {
        int[] a = new int[]{2, 1, 2, 2, 3, 3, 3, 3, 3, 3};
        HashMap<Integer, Integer> numbers = new HashMap<>();

        Integer k, v; // key, value
        for (int i = 0; i < a.length; i++) {
            k = a[i];
            v = numbers.get(k);
            numbers.put(k, (v == null ? 1 : (v + 1)));
            if ((numbers.get(k) > a.length / 2)) {
                System.out.println(k);
                break;
            }
        }
    }
}
