/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 1
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair3;

import java.util.HashMap;
import java.util.Map;

public class UniqueNumbers {
    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 1, -4, 2, 2};
        HashMap<Integer, Integer> numbers = new HashMap<>();

        Integer k;  // key, value
        Integer v;
        for (int i = 0; i < a.length; i++) {
            k = a[i];
            v = numbers.get(k);
            numbers.put(k, (v == null ? 1 : (v + 1)));
        }

        int countUniqNums = 0;
        for (Map.Entry entry : numbers.entrySet()) {
            if (entry.getValue().equals(1)) countUniqNums++;
        }
        System.out.println(countUniqNums);
    }
}
