/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair4;

import java.util.Arrays;

public class Permutations {

    static char[] a = new char[]{'1', '2', '3'};
    static int N = a.length;

    public static void main(String[] args) {
        System.out.print(generate_permutations());
    }

    public static StringBuilder generate_permutations() {
        StringBuilder p = new StringBuilder();
        int i, j, numberPermutations = fact();

        p.append(Arrays.toString(a) + "\n");
        for (j = 1; j < numberPermutations; j++) {
            // поиск ПЕРВОЙ пары элементов справа : левый < правого
            // (находим индекс левого элемента в паре)
            for (i = N - 2; a[i] > a[i + 1]; i--) ;

            // сортируем правую часть массива по возрастанию.
            // справа от a[i] элементы расположены по убыванию,
            // поэтому обратим часть справа от a[i] (это наше сортирующее действие)
            reverseRightPart(i + 1);

            // выполняем 1 транспозицию (свапаем)
            // поиск МЕНЬШЕГО из БОЛЬШИХ i - го элементов справа
            finish(i);

            p.append(Arrays.toString(a) + "\n");
        }
        return p;
    }

    private static void finish(int i) {
        int k = i + 1;
        while (a[i] > a[k]) k++;
        swap(i, k);
    }

    private static void reverseRightPart(int pos) {
        int lengthPart = N - pos, k = pos;

        for (int i = 0; i < lengthPart / 2; i++) {
            swap(k, N - i - 1);
            k++;
        }
    }

    private static void swap(int i, int j) {
        char t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    private static int fact() {
        int fact = 1;
        for (int i = 1; i <= N; i++) fact *= i;
        return fact;
    }
}