/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair4;

import java.util.LinkedHashSet;
import java.util.Set;

public class RepeatingElemSubsets {
    public static void main(String[] args) {
        Character[] a = {'1', '2', '2'};
        int N = a.length;
        StringBuilder subset;
        Set<StringBuilder> set = new LinkedHashSet<>();
        // алгоритм на основан на представлении подмножеств в виде двоичного набора чисел
        // для раскодирования двоичного представления подмножеств используются битовые маски и битовые операции
        for (int bitMask = 0; bitMask < (1 << N); bitMask++) {
            subset = new StringBuilder();
            for (int i = 0; i < N; i++) {
                if ((bitMask & (1 << i)) != 0) {
                    subset.append(a[i] + " ");
                }
            }
            set.add(subset);
        }
        for (StringBuilder subs : set) {
            System.out.println(subs);
        }
    }
}
