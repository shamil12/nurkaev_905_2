/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair4;

public class UniqueSubset {
    public static void main(String[] args) {
        char[] a = {'1', '2', '3'};
        int N = a.length;
        StringBuilder subset;
        // алгоритм на основан на представлении подмножеств в виде двоичного набора чисел
        // для раскодирования двоичного представления подмножеств используются битовые маски
        for (int bitMask = 0; bitMask < (1 << N); bitMask++) {
            subset = new StringBuilder();
            for (int i = 0; i < N; i++) {
                if ((bitMask & (1 << i)) != 0) { // раскодирование двоичного представления
                    subset.append(a[i] + " ");
                }
            }
            System.out.println(subset.toString());
        }
    }
}
