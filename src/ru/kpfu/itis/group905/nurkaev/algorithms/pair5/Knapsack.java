/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair5;

import java.util.Stack;

public class Knapsack {

    // Пусть F(i,k) - максимальная стоимость предметов, которые можно
    // уложить в рюкзак ёмкостью k, если можно использовать только первые i предметов, т.е. {1,..,i}.
    // Если предметов нет, то максимальная стоимость = 0, т.е. F(0,k) = 0.
    // Если ёмкость рюкзака = 0, то максимальная стоимость = 0, т.е. F(i,0) = 0.
    //
    // Идея решения.
    // Найдём F(i,k). Взможно 2 варианта:
    // 1) Предмет i попал в рюкзак.
    //    Тогда F(i,k) = V(i) + F(i-1,k-D(i));
    // 2) Предмет i не попал в рюкзак.
    //    Тогда F(i,k) = F(i-1,k);
    //
    // Следовательно, F(i,k) = max(V(i) + F(i-1)(k-D(i)), F(i-1)(k)).
    //
    // Ответ: F[кол-во предметов][B].

    // ПРЕДМЕТЫ ДЛЯ УДОБСТВА НУМЕРУЮТСЯ С ЕДИНИЦЫ {1,..,i,..,N}, поэтому в коде это нужно учитывать


    public static void main(String[] args) {
        int B = 10; // ёмкость рюкзака
        int[] D = {4, 4, 3, 5}; // веса грузов
        int[] V = {1, 5, 3, 4}; // стоимости грузов
        int N = D.length;
        int[][] F = new int[N + 1][B + 1];

        for (int i = 1; i < N + 1; i++) {
            for (int k = 1; k < B + 1; k++) {
                if (D[i - 1] <= k) {
                    F[i][k] = Integer.max(V[i - 1] + F[i - 1][k - D[i - 1]], F[i - 1][k]);
                } else {
                    F[i][k] = F[i - 1][k];
                }
            }
        }

        System.out.println("Максимальная стоимость грузов, которые вмещаются в рюкзак: " + F[N][B]);

        System.out.print("Номера предметов, которые нужно поместить в рюкзак: ");
        Stack<Integer> stack = new Stack<>();
        int k = B;
        for (int i = N; i > 0; i--) {
            if (F[i][k] != F[i - 1][k]) {
                stack.push(i);
                k -= D[i - 1];
            }
        }
        while (!stack.isEmpty()) System.out.print(stack.pop() + " ");
    }
}
