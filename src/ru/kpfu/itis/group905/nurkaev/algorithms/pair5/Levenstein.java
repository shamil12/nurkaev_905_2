/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair5;

public class Levenstein {
    public static void main(String[] args) {
        char[] A = "молоко".toCharArray();
        char[] B = "колокол".toCharArray();
        int N = A.length, M = B.length;
        int[][] D = new int[N + 1][M + 1];

        for (int i = 0; i < N; i++) D[i][0] = i;
        for (int j = 0; j < M; j++) D[0][j] = j;

        for (int i = 1; i < N + 1; i++) {
            for (int j = 1; j < M + 1; j++) {
                if (A[i - 1] == B[j - 1]) {
                    D[i][j] = D[i - 1][j - 1];
                } else {
                    D[i][j] = 1 + min(D[i - 1][j], D[i][j - 1], D[i - 1][j - 1]);
                }
            }
        }

        System.out.println(D[N][M]);
    }

    private static int min(int a, int b, int c) {
        return Integer.min(Integer.min(a, b), c);
    }
}