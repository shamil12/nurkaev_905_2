/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair5;

import java.util.Stack;

public class LongestIncreasingSubsequence {

    // Пусть L(i) - длина наиб. возр. подпоследовательности, последний элемент которой a(i).
    // Слева от a(i) есть какое-то число a(j) (a(j) < a(i), j = 1,..,i-1) с длиной L(j).
    // Тогда L(i) = L(j) + 1.
    // Таким образом, среди всех a(j) нужно найти такое число, что:
    // 1) a(j) < a(i)
    // 2) L(j) = max(L(k)), k = 1,..,i-1
    // Наибольшая длина искомой подпослед. = max(L(i))

    // Получить одну из таких подпоследовательностей можно так.
    // Если L(i) - один из максимумов среди всех L(i), то подпослед. оканчивается на a(i).
    // Значит, предпоследнее число в этой подпослед. меньше a(i) и имеет длину L(i) - 1. И т.д.

    public static void main(String[] args) {
        int[] a = {1, -10, -5, -20, -0, -24, -7};

        int maxL = 0;
        int[] L = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < i; j++) {
                if (a[j] < a[i] && L[j] > L[i]) {
                    L[i] = L[j];
                }
            }
            L[i] += 1;

            if (L[i] > maxL) {
                maxL = L[i];
            }
        }

        int l = maxL;
        Stack<Integer> LIS = new Stack<>();
        for (int i = a.length - 1; i >= 0 && l >= 1; i--) {
            if (L[i] == l) {
                LIS.push(a[i]);
                l--;
            }
        }

        System.out.println("Longest increasing subsequence: " + maxL);
        System.out.print("An example of such a subsequence: ");
        while (!LIS.isEmpty()) System.out.print(LIS.pop() + " ");
    }
}
