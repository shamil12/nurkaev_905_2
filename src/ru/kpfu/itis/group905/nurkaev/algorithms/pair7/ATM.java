/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair7;

import java.util.Scanner;

public class ATM {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] notes = {5000, 1000, 500, 100, 50}; // массив должен быть отсортирован по убыванию

        if (N < notes[notes.length - 1]) {
            System.out.println(0);
            return;
        }

        int countNotes = 0;
        for (int i = 0; i < notes.length && N > 0; i++) {
            while (N >= notes[i]) {
                N -= notes[i];
                countNotes += 1;
            }
        }

        System.out.println(countNotes);
    }
}
