/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair7;

public class MaximumSumContinuousSubsequence {
    public static void main(String[] args) {
        int[] numbers = {-3, -4, -10, -2};

        int maxS = numbers[0];
        int partialSum = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            partialSum = Math.max(partialSum + numbers[i], numbers[i]);
            maxS = Math.max(maxS, partialSum);
        }

        System.out.println(maxS);
    }
}