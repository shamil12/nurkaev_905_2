/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 * Task 4
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair8;

public class ChangedArray {
    public static void main(String[] args) {
        int[] a = {1, 4, 5, 2, 3};
        System.out.println(getIndex(a, 3));
    }

    private static int getIndex(int[] a, int k) {
        int N = a.length;
        int m = 1;
        int l = 0;
        int r = N - 1;
        int iShift;

        // за O(log(n)) находим индекс числа, с которого нарушается возрастание
        while (a[m] > a[m - 1]) {
            m = (l + r) / 2;
            if (a[m] > a[N - 1]) l = m + 1;
            else r = m - 1;
        }
        iShift = m;

        // Если искомый элемент находится в пределах правой части,
        // запускаем бинарный поиск справа.
        // В противном случае - ищем в левой части.

        // В худшем случае в части, в которой продолжится поиск, останется n-1 элементов.
        // Поэтому искомый индекс будет найден за O(log(n-1))

        // ОБЩАЯ СЛОЖНОСТЬ: O(log(n) + log(n-1))
        if (k >= a[iShift] && k <= a[N - 1]) {
            l = iShift;
            r = N - 1;
        } else {
            l = 0;
            r = iShift - 1;
        }
        while (l <= r) {
            m = (l + r) / 2;
            if (a[m] < k) l = m + 1;
            else if (a[m] > k) r = m - 1;
            else break;
        }

        return m;
    }
}
