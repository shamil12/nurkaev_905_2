/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 * Task 3
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair8;

import java.util.Arrays;

public class FirstLastOccurence {
    public static void main(String[] args) {
        int[] a = {5, 6, 7, 8, 8, 8, 9};
        a = getIndexes_firstLastOccurence(a, 8);
        System.out.println(Arrays.toString(a));
    }

    private static int[] getIndexes_firstLastOccurence(int[] a, int k) {
        int N = a.length;

        int m;
        int l = 0;
        int r = a.length - 1;

        while (l <= r) {
            m = (l + r) / 2;
            if (k > a[m]) l = m + 1;
            else if (k < a[m]) r = m - 1;
            else {
                int first = m;
                int last = m;
                while (first >= 0 && a[first] == a[m]) first--;
                while (last <= N - 1 && a[last] == a[m]) {
                    last++;
                }
                return new int[]{first + 1, last - 1};
            }
        }

        return new int[]{-1, -1};
    }
}
