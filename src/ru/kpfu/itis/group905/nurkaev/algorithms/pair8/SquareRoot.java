/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 * Task 1
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair8;

public class SquareRoot {
    public static void main(String[] args) {
        int x = 99;
        System.out.println(approximateSqrt(x));
    }

    private static int approximateSqrt(int x) {
        if (x < 0) return -1;
        if (x == 0) return 0;

        int k = 1;
        while (k * k <= x) {
            if ((k + 1) * (k + 1) > x) break;
            k++;
        }
        return k;
    }
}