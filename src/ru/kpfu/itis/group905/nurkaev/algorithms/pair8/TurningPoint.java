/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 * Task 2
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair8;

public class TurningPoint {
    public static void main(String[] args) {
        int[] a = {4, 3, 2, 1, 0, 2, 3};
        System.out.println(getIndex_turningPoint(a));
    }

    private static int getIndex_turningPoint(int[] a) {
        int m;
        int l = 0;
        int r = a.length - 1;

        while (l <= r) {
            m = (l + r) / 2;
            if (a[m] > a[m + 1]) l = m + 1;
            else if (a[m] > a[m - 1]) r = m - 1;
            else if (a[m] < a[m - 1] && a[m] < a[m + 1]) {
                return m;
            }
        }

        return l;
    }
}
