/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair9;

public class Task1 {
    public static void main(String[] args) {
        String s = "trixma";
        String t = "matrix";

        System.out.println(cyclicalShift(s, t));
    }

    /**
     * Метод проверяет является ли строка s циклическим сдвигом ВЛЕВО от строки t.
     * Возвращает число символов строки t, переставленных в конец строки s.
     * Будет возвращён 0, если s совпадает с t или не является циклическим сдвигом.
     */
    private static int cyclicalShift(String s, String t) {
        if (s.equals(t) || s.length() != t.length()) return 0;
        String temp = t.concat(t);
        int i = 0;
        for (; i < s.length() && s.equals(temp.substring(i, i + t.length())); i++) ;
        return i;
    }

}
