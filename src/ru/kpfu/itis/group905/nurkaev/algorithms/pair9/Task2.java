/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair9;

public class Task2 {
    public static void main(String[] args) {
        String A = " the sky is blue  ";
        A = A.trim();

        StringBuilder B = new StringBuilder();
        for (int i = A.length() - 1; i >= 0; i--) {
            StringBuilder temp = new StringBuilder();
            while (i >= 0 && A.charAt(i) != ' ') {
                temp.append(A.charAt(i--));
            }
            B.append(temp.reverse()).append(" ");
        }

        System.out.println(B);
    }
}