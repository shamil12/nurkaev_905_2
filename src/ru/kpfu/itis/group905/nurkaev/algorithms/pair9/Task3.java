/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair9;

public class Task3 {
    public static void main(String[] args) {
        System.out.println(atoi("42"));
        System.out.println(atoi("3.14159"));
        System.out.println(atoi("-31337 with words"));
        System.out.println(atoi("-51234582135"));
        System.out.println(atoi("words and 2"));
        System.out.println(atoi("-"));
    }

    private static int atoi(String s) {
        if (s.isEmpty() || !s.matches("[-\\d].*")) {
            return 0;
        }

        int i = 0;
        boolean negative = false;
        if (s.charAt(0) == '-') negative = true;
        if (s.length() > 1 && negative) i = 1;

        long result = 0;
        while (i < s.length() && s.charAt(i) >= '0' && s.charAt(i) <= '9') {
            result = result * 10 + s.charAt(i++) - '0';
            if (result > Integer.MAX_VALUE) return Integer.MAX_VALUE;
            if (result < Integer.MIN_VALUE) return Integer.MIN_VALUE;
        }

        return negative ? (int) -result : (int) result;
    }
}