/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.pair9;

public class Task4 {
    public static void main(String[] args) {
        System.out.println(printJSON("{A:\"B\",C:{D:\"E\",F:{G:\"H\",I:\"J\"}}}"));
//        System.out.println(printJSON("[\"foo\",{\"bar\":[\"baz\",null,1.0,2]}]"));
    }

    private static String printJSON(String A) {
        StringBuilder res = new StringBuilder();

        int openBrackets = 0;
        for (int i = 0; i < A.length(); i++) {
            char c = A.charAt(i);
            if (c == '{') {
                res.append("\n").append("\t".repeat(openBrackets));
                res.append("{\n").append("\t".repeat(++openBrackets));
            } else if (c == '[') {
                res.append("\n").append("\t".repeat(openBrackets));
                res.append("[\n").append("\t".repeat(++openBrackets));
            } else if (c == '"') res.append("\"");
            else if (c == ',') {
                res.append(",");
                if (A.charAt(i+1) != '{' && A.charAt(i+1) != '[') {
                    res.append("\n");
                }
                res.append("\t".repeat(openBrackets));
            } else if (c == '}') {
                res.append("\n").append("\t".repeat(--openBrackets));
                res.append("}");
            } else if (c == ']') {
                res.append("\n").append("\t".repeat(--openBrackets));
                res.append("]");
            } else res.append(c);
        }

        return res.deleteCharAt(0).toString();
    }
}