/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Sem 1
 * Testing of Polinom3
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.sem1;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String filename = "C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/algorithms/sem1/polinom.txt";
        String filename_add = "C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/algorithms/sem1/polinom2.txt";
        Polinom3 p = new Polinom3(filename);
        System.out.println(p);

//        p.insert(-1, 2, 1, 1);
//        System.out.println(p);
//
//        p.delete(2, 2, 2);
//        System.out.println(p);

//        System.out.println(p2);
//        p.add(p2);
//        System.out.println(p);

        p.derivate(3);
        System.out.println(p);

//        System.out.println(p.value(1, 1, 0));
    }
}
