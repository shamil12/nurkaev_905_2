/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Sem 1
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.sem1;

public class Monom {
    public int coef;
    public int deg1;
    public int deg2;
    public int deg3;

    public Monom(int coef, int deg1, int deg2, int deg3) {
        this.coef = coef;
        this.deg1 = deg1;
        this.deg2 = deg2;
        this.deg3 = deg3;
    }

    public boolean equals(Monom m) {
        if (coef != m.coef) return false;
        if (deg1 != m.deg1) return false;
        if (deg2 != m.deg2) return false;
        if (deg3 != m.deg3) return false;
        return true;
    }
}
