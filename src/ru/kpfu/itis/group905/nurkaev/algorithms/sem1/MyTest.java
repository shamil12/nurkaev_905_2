/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Sem 1
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.sem1;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;

public class MyTest {
    String path = "C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/algorithms/sem1/";
    Polinom3 p;

    @Before
    public void setUp() throws FileNotFoundException {
        p = new Polinom3(path + "polinom.txt");
    }

    @Test
    public void positiveTest_insert() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_insert.txt");
        p2.insert(3, 2, 2, 2);
        testing(p2);
    }

    @Test
    public void negativeTest_insert() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_insert.txt");
        p2.insert(2, 2, 2, 2);
        testing(p2);
    }

    @Test
    public void positiveTest_delete() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_delete.txt");
        p2.delete(0, 0, 0);
        testing(p2);
    }

    @Test
    public void negativeTest_delete() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_delete.txt");
        p2.delete(0, 0, 1);
        testing(p2);
    }

    @Test
    public void positiveTest_add() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_add.txt");
        Polinom3 p3 = new Polinom3(path + "p_add2.txt");
        p2.add(p3);
        testing(p2);
    }

    @Test
    public void negativeTest_add() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_add.txt");
        Polinom3 p3 = new Polinom3(path + "p_add2.txt");
        p2.add(p3);
        p2.add(p3);
        testing(p2);
    }

    @Test
    public void positiveTest_derivate() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_derivate.txt");
        p2.derivate(3);
        testing(p2);
    }

    @Test
    public void negativeTest_derivate() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3(path + "p_derivate.txt");
        p2.derivate(1);
        testing(p2);
    }

    @Test
    public void positiveTest_value() throws FileNotFoundException {
        int value = p.value(1, 1, 0);
        if (value != 1) System.out.println("Expected: " + 1 + "\n" + "Actual:   " + value);
        else System.out.println("true");
    }

    @Test
    public void negativeTest_value() throws FileNotFoundException {
        int value = p.value(1, 1, 1);
        if (value != 1) System.out.println("Expected: " + 1 + "\n" + "Actual:   " + value);
        else System.out.println("true");
    }

    private void testing(Polinom3 p2) {
        if (!p.equals(p2)) System.out.println("Expected: " + p + "\n" + "Actual:   " + p2);
        else System.out.println("true");
    }
}