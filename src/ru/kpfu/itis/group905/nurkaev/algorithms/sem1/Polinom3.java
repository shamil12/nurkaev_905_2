/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Sem 1
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.sem1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Polinom3 {
    public MyLinkedList<Monom> monoms = new MyLinkedList<>();

    public Polinom3(String filename) throws FileNotFoundException {
        Scanner scan = new Scanner(new File(filename));
        int coef, deg1, deg2, deg3, k;
        while (scan.hasNext()) {
            k = scan.nextInt();
            boolean isNegative = false;
            if (k / 1000 == 0) continue; // не создаём одночлен с нулевым коэффициентом
            if (k < 0) {
                isNegative = true;
                k *= -1;
            }
            deg3 = k % 10;
            k /= 10;
            deg2 = k % 10;
            k /= 10;
            deg1 = k % 10;
            k /= 10;
            if (isNegative) k *= -1;
            coef = k;
            monoms.add(new Monom(coef, deg1, deg2, deg3));
        }
    }

    public void insert(int coef, int deg1, int deg2, int deg3) {
        Monom m1 = new Monom(coef, deg1, deg2, deg3);
        if (!monoms.contains(m1) && coef != 0) {
            int i = 0;
            Monom m2 = monoms.get(i);
            int a = Integer.parseInt("" + m1.deg1 + m1.deg2 + m1.deg3);
            int b = Integer.parseInt("" + m2.deg1 + m2.deg2 + m2.deg3);

            while (a <= b && i < monoms.size()) {
                m2 = monoms.get(i);
                b = Integer.parseInt("" + m2.deg1 + m2.deg2 + m2.deg3);
                i++;
            }
            if (a >= b) monoms.insertLeft(m1, m2);
            else monoms.add(m1);
        }
    }

    public void delete(int deg1, int deg2, int deg3) {
        for (int i = 0; i < monoms.size(); i++) {
            Monom m = monoms.get(i);
            if (m.deg1 == deg1 && m.deg2 == deg2 && m.deg3 == deg3) monoms.remove(m);
        }
    }

    public void add(Polinom3 p) {
        MyLinkedList<Monom> monoms2 = new MyLinkedList<>();
        int i = 0, j = 0;
        Monom m1, m2;
        while (i < monoms.size() && j < p.monoms.size()) {
            m1 = monoms.get(i);
            m2 = p.monoms.get(j);
            int a = Integer.parseInt("" + m1.deg1 + m1.deg2 + m1.deg3);
            int b = Integer.parseInt("" + m2.deg1 + m2.deg2 + m2.deg3);
            if (a > b) {
                monoms2.add(m1);
                i++;
            } else {
                monoms2.add(m2);
                j++;
            }
        }
        while (i < monoms.size()) {
            m1 = monoms.get(i++);
            monoms2.add(m1);
        }
        while (j < p.monoms.size()) {
            m2 = p.monoms.get(j++);
            monoms2.add(m2);
        }
        monoms = monoms2;
    }

    public void derivate(int i) {
        if (i == 1) for (int j = 0; j < monoms.size(); j++) {
            Monom m = monoms.get(j);
            if (m.deg1 == 0) monoms.remove(m); // считаем одночлен за константу, если в него не входит i-я переменная
            m.coef *= m.deg1;
            m.deg1 -= 1;
        }
        else if (i == 2) for (int j = 0; j < monoms.size(); j++) {
            Monom m = monoms.get(j);
            if (m.deg2 == 0) monoms.remove(m);
            m.coef *= m.deg2;
            m.deg2 -= 1;
        }
        else if (i == 3) for (int j = 0; j < monoms.size(); j++) {
            Monom m = monoms.get(j);
            if (m.deg3 == 0) monoms.remove(m);
            m.coef *= m.deg3;
            m.deg3 -= 1;
        }
    }

    public int value(int x, int y, int z) {
        for (int i = 0; i < monoms.size(); i++) {
            Monom m = monoms.get(i);
            m.coef *= pow(x, m.deg1) * pow(y, m.deg2) * pow(z, m.deg3);
            m.deg1 = 0;
            m.deg2 = 0;
            m.deg3 = 0;
        }
        int value = 0;
        for (int i = 0; i < monoms.size(); i++) {
            value += monoms.get(i).coef;
        }
        return value;
    }

    private int pow(int x, int deg) {
        if (x == 0 && deg != 0) return 0;
        if (deg == 0) return 1;

        int k = 1;
        for (int i = 0; i < deg; i++) {
            k *= x;
        }
        return k;
    }

    @Override
    public String toString() {
        String p = "";
        for (int i = 0; i < monoms.size(); i++) {
            Monom m = monoms.get(i);

            // знак следующего монома
            String sign_next;
            if (i != monoms.size() - 1) {
                sign_next = signCoef(monoms.get(i + 1).coef);
            } else sign_next = "";

            String correct_coef; // правильное строковое представление коэффициента первого монома и абсолютное значение других
            int coef = Math.abs(m.coef);
            if (m.coef == 1 || m.coef == -1) correct_coef = ((i == 0) ? ((m.coef == 1) ? "" : "- ") :
                    (((m.deg1 == 0) && (m.deg2 == 0) && (m.deg3 == 0)) ? "1" : ""));
            else correct_coef = ((i == 0) ? ((m.coef > 0) ? ("" + coef) :
                    ("- " + coef)) : ("" + coef));
            p += getString(m, correct_coef, sign_next);
        }
        return p;
    }

    private String getString(Monom m, String c, String s) {
        String deg1 = deg_isEqual_one(m.deg1);
        String deg2 = deg_isEqual_one(m.deg2);
        String deg3 = deg_isEqual_one(m.deg3);
        if (m.deg1 == 0 && m.deg2 == 0 && m.deg3 == 0) return (c + s);
        else if (m.deg1 == 0 && m.deg2 == 0) return (c + "z" + deg3 + s);
        else if (m.deg1 == 0 && m.deg3 == 0) return (c + "y" + deg2 + s);
        else if (m.deg2 == 0 && m.deg3 == 0) return (c + "x" + deg1 + s);
        else if (m.deg1 == 0) return (c + "y" + deg2 + "*z" + deg3 + s);
        else if (m.deg2 == 0) return (c + "x" + deg1 + "*z" + deg3 + s);
        else if (m.deg3 == 0) return (c + "x" + deg1 + "*y" + deg2 + s);
        else return (c + "x" + deg1 + "*y" + deg2 + "*z" + deg3 + s);
    }

    private String deg_isEqual_one(int deg) {
        if (deg == 1) return "";
        return "^" + deg;
    }

    private String signCoef(int a) {
        if (a < 0) return " - ";
        return " + ";
    }

    public boolean equals(Polinom3 p) {
        if (monoms.size != p.monoms.size()) return false;
        for (int i = 0; i < monoms.size(); i++) {
            if (!monoms.get(i).equals(p.monoms.get(i)))
                return false;
        }
        return true;
    }
}