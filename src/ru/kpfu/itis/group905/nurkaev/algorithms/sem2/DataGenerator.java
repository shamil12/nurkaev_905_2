/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.sem2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.Random;

public class DataGenerator {

    private static final String PATH =
            "src/ru/kpfu/itis/group905/nurkaev/algorithms/sem2/Strings.txt";

    static void createFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(PATH))) {
            Random rnd = new Random();
            for (int step = 0; step < 50; step++) {
                String s = getRandomString(rnd, 4900);
                String pattern = getRandomString(rnd, 1);
                writer.write(s + "," + pattern + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static LinkedList<String[]> readFile() {
        LinkedList<String[]> strings = new LinkedList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(PATH))) {
            while (reader.ready())
                strings.add(reader.readLine().split(","));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strings;
    }

    private static String getRandomString(Random rnd, int maxlen) {
        int n = rnd.nextInt(maxlen) + 100;
        char[] s = new char[n];
        for (int i = 0; i < n; i++)
            s[i] = (char) ('a' + rnd.nextInt(3));
        return new String(s);
    }
}
