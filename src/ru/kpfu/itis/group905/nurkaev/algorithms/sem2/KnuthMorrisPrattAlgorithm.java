/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Sem 2
 */

package ru.kpfu.itis.group905.nurkaev.algorithms.sem2;

import java.io.IOException;
import java.util.LinkedList;

public class KnuthMorrisPrattAlgorithm {

    private static int iteration_count;

    public static void main(String[] args) throws IOException {
        DataGenerator.createFile(); // генерация входных данных в файле
        LinkedList<String[]> strings =
                DataGenerator.readFile(); // считывание данных в список

        for (String[] line : strings) {
            String s = line[0];
            String pattern = line[1];

            long start = System.nanoTime();
            int pos = kmpMatcher(s, pattern); // поиск подстроки в строке
            long end = System.nanoTime();
            long timeConsumedNano = end - start; // время работы алгоритма

            System.out.println(String.format(
                    "time consumed nano: %d, iteration count: %d\n%s, %s, %s\n",
                    timeConsumedNano, iteration_count, pos, s, pattern));
            iteration_count = 0;
        }
    }


    public static int kmpMatcher(String s, String pattern) {
        int len = pattern.length();
        if (len == 0)
            return 0;
        int[] pi = prefixFunction(pattern); // longest suffix-prefix
        for (int i = 0, k = 0; i < s.length(); i++)
            for (; ; k = pi[k - 1]) {
                iteration_count++;
                if (pattern.charAt(k) == s.charAt(i)) {
                    if (++k == len)
                        return i - len + 1;
                    break;
                }
                if (k == 0)
                    break;
            }
        return -1;
    }

    public static int[] prefixFunction(String s) {
        int[] pi = new int[s.length()];
        int k = 0;
        for (int i = 1; i < s.length(); i++) {
            while (k > 0 && s.charAt(k) != s.charAt(i)) {
                k = pi[k - 1];
                iteration_count++;
            }
            if (s.charAt(k) == s.charAt(i)) {
                k++;
            }
            pi[i] = k;
        }
        return pi;
    }
}