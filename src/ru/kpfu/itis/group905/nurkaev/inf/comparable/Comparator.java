/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

public class Comparator implements java.util.Comparator<Integer> {

    @Override
    public int compare(Integer a, Integer b) {
        while (true) {
            a /= 10;
            b /= 10;
            if (a == 0 && b == 0) return 0;
            else if (a == 0) return -1;
            else if (b == 0) return 1;
        }
    }
}
