/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 4
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

public class Comparator2 implements java.util.Comparator<Integer> {

    @Override
    public int compare(Integer a, Integer b) {
        int mirrored1 = 0, mirrored2 = 0;
        while (a != 0) {
            mirrored1 = mirrored1 * 10 + a % 10;
            a /= 10;
        }
        while (b != 0) {
            mirrored2 = mirrored2 * 10 + b % 10;
            b /= 10;
        }
        if (mirrored1 > mirrored2) return 1;
        else if (mirrored1 < mirrored2) return -1;
        return 0;
    }
}
