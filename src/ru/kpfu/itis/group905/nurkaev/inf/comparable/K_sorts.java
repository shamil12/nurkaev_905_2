/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 1
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class K_sorts {
    public static void main(String[] args) throws IOException {
        File file = new File("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/in.txt");
        Scanner scan = new Scanner(file);
        ArrayList<String> lines = new ArrayList<>();

        int k = scan.nextInt();
        scan.nextLine();
        while (scan.hasNext()) {
            String s = scan.next();
            if (s.length() >= k) lines.add(s);
        }
        scan.close();

        PrintWriter out = new PrintWriter("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/out.txt");

        for (int i = 0; i < k; i++) {
            int index = i;
            Collections.sort(lines, Comparator.comparing(x -> x.charAt(index)));
            for (String s : lines) {
                out.print(s + " ");
            }
            out.println();
        }
        out.close();
    }
}
