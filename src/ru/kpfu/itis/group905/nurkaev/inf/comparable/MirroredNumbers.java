/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 4
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MirroredNumbers {
    public static void main(String[] args) {
        ArrayList<Integer> lines= new ArrayList<>(Arrays.asList(123, 51, 70, 25, 19));
        Collections.sort(lines, new Comparator2());
        System.out.println(lines.toString());
    }
}
