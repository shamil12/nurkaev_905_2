/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 2
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) throws IOException {
        File file = new File("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/in.txt");
        Scanner scan = new Scanner(file);
        ArrayList<Integer> lines = new ArrayList<>();

        while (scan.hasNext()) {
            lines.add(scan.nextInt());
        }
        scan.close();

        PrintWriter out = new PrintWriter("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/out.txt");

        Collections.sort(lines, new Comparator());
        out.println(lines.toString());
        out.close();
    }
}
