/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 3
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

public class SitesData implements Comparable<SitesData> {
    public String name;
    public double[] visitors;

    public SitesData(String name, double[] visitors) {
        this.name = name;
        this.visitors = visitors;
    }

    @Override
    public int compareTo(SitesData s) {
        return this.name.compareTo(s.name);
    }
}
