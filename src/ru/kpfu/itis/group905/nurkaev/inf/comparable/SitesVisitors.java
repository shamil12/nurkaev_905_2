/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 3
 */

package ru.kpfu.itis.group905.nurkaev.inf.comparable;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SitesVisitors{
    public static void main(String[] args) throws IOException {
        File file = new File("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/in.txt");
        Scanner scan = new Scanner(file);
        ArrayList<SitesData> lines = new ArrayList<>();

        while (scan.hasNextLine()) {
            String name = scan.next();

            double[] visitors = new double[7];
            for (int i = 0; i < 7; i++) {
                visitors[i] = scan.nextDouble();
            }

            lines.add(new SitesData(name, visitors));
        }
        scan.close();

        PrintWriter out = new PrintWriter("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/out.txt");
        for (SitesData s : lines) {
            out.print(s.name + " " + average(s.visitors) + "\n");
        }
        out.close();

        PrintWriter out2 = new PrintWriter("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/comparable/out2.txt");
        Collections.sort(lines);
        for (SitesData s : lines) {
            out2.print(s.name + " " + average(s.visitors) + "\n");
        }
        out2.close();
    }

    private static Double average(double[] visitors) {
        double sum = 0;
        for (Double s : visitors) {
            sum += s;
        }
        return sum / 7;
    }
}
