/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Convert {
    public static final double FOOT = 30.48;
    public static final double INCH = 2.54;

    public static void convert() {
        Scanner scanner = new Scanner(System.in);
        int foot = 0, inch = 0;
        try {
            System.out.print("Enter height in feet: ");
            foot = scanner.nextInt();
            System.out.print("and in inches: ");
            inch = scanner.nextInt();

            while (foot < 0 || inch < 0) {
                System.out.println("Please enter positive values only.");
                System.out.print("Enter height in feet: ");
                foot = scanner.nextInt();
                System.out.print("and in inches: ");
                inch = scanner.nextInt();

                double result = foot * FOOT + inch * INCH;
                System.out.println("Result: " + result + "cm");
            }
        } catch (InputMismatchException e) {
            System.out.println("You must enter integers. Please try again.");
        } finally {
            if (foot == 0 && inch == 0) convert();
        }
    }

    public static void main(String[] args) {
        convert();
    }
}
