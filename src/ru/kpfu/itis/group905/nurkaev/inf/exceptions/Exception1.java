/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

// Для создания собственного класса исключений необходимо
// объявить его производным от уже существующего типа.

// В блоке try принудительно иницируется исключение
// с помощью передачи оператору throw ссылки на объект с информацией об ошибке.
// Далее управление передаётся в "ближайший" подходящий обработчик исключений (catch - полиморфен)
// (выполняется только одна секция catch, соответствующая типу исключения).
// Затем выполняется секция блока finally, т.н. заверщающий код (действие производится в любом случае).

public class Exception1 extends Exception {
    public static void main(String[] args) {
        try {
            throw new Exception1();
        } catch (Exception1 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }
    }
}