/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

// Компилятор заставляет писать код для обработки прямо на месте.
// Вот несколько способов для решения проблемы.

// 1) С помощью ключевого слова throws объявить в сигнатуре о возбуждении исключения (так я и сделал).
// 2) Распечатать в обработчике трассировку стека исключения.
// 3) "Проглотить исключение". Передать управление в "пустой" (без обрабатывающего предложения) блок catch.

public class Exception2 extends Exception {
    public static void main(String[] args) throws Exception2 {
        try {
            throw new Exception2();
        } finally {
            System.out.println("c");
        }
    }
}

//public class Exception2 extends Exception {
//    public static void main(String[] args) {
//        try {
//            throw new Exception2();
//        } catch (Exception2 exception2) {
//            exception2.printStackTrace();
//        } finally {
//            System.out.println("c");
//        }
//    }
//}

//public class Exception2 extends Exception {
//    public static void main(String[] args) {
//        try {
//            throw new Exception2();
//        } catch (Exception2 exception2) {
//        } finally {
//            System.out.println("c");
//        }
//    }
//}
