/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

// При повторном возбуждении (19) исключение передаётся обработчику более высокого уровня.
// Все остальные предложения catch текущего блока try игнорируются.
// Пример исправленной программы ниже.

public class Exception3 {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("a");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println("b");
            throw new Exception();
        } catch (Exception e) {
            System.out.println("c");
        }
    }
}

//public class Exception3 {
//    public static void main(String[] args) throws Exception {
//        try {
//            try {
//                System.out.println("a");
//                throw new NullPointerException();
//            } catch (NullPointerException e) {
//                System.out.println("b");
//                throw new Exception();
//            }
//        }catch (Exception e) {
//            System.out.println("c");
//        }
//    }
//}
