/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

// Вначале выводится символ "a". Далее выполняется код защищенной секции (try-catch).
// В блоке try не выбрасываются исключения, поэтому выведется символ "b".
// Далее, минуя catch, выполняется завершающий код - вывод "d".
// В конце выведется "e".

public class Exception4 {
    public static void main(String[] args) {
        System.out.println("a");
        try {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
