/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

// Возвращаемое значение перезаписывается другим значением в завершающем коде.
// Поэтому независимо от результата divide() возвращает 10 (нужно удалить блок finally).

public class Exception5 {
    public static int divide(int a, int b) {
        try {
            return a / b;
        } catch (Exception e) {
            return 0;
        } finally {
            return 10;
        }
    }

    public static void main(String[] args) {
        int i = divide(11, 1);
        System.out.println(i);
    }
}
