/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

// Вначале выводится "Hi". Далее выполняется код защищенной секции (try-catch).
// Далее выполняется код защищенной секции (try-catch). Выводится "a",
// после выбрасывается исключение, и обрабатывается первым обработчиком с соотв. типом аргумента.
// Выводится "b" и в завершающем коде печатается "d".
// В конце выведется "e".

public class Exception6 extends Exception {
    public static void main(String[] args) {
        System.out.println("Hi!");
        try {
            System.out.println("a");
            throw new Exception6();
        } catch (Exception6 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
