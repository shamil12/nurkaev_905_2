/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

import java.util.Scanner;

public class IDNumber{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();

        try {
            int number = Integer.parseInt(s);
            System.out.println("Input = " + number + "\n" + "Output: correct");
        } catch (NumberFormatException e) {
            System.out.println("Output: inccorect");
        }
    }
}