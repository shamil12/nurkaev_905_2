/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

import java.util.Scanner;

public class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static void main(String[] args) throws PointException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter x coordinate of current point: ");
        int x = scanner.nextInt();
        System.out.print("Enter y coordinate of current point: ");
        int y = scanner.nextInt();
        Point point = new Point(x, y);

        System.out.print("Enter x coordinate of target point: ");
        int num3 = scanner.nextInt();
        System.out.print("Enter y coordinate of target point: ");
        int num4 = scanner.nextInt();

        System.out.println(point.slope(num3, num4));
    }

    public int slope(int num3, int num4) throws PointException {
        if (x == num3) {
            System.out.println("Straight angle = 90 degrees");
            System.exit(42);
        } else if (y == num4) {
            System.out.println("Straight angle = 0 degrees");
            System.exit(42);
        } else if (x == num3 && y == num4) {
            throw new PointException("The same point entered");
        }
        return (y - num4) / (x - num3);
    }
}

