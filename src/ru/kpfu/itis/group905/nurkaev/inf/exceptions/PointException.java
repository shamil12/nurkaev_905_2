/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

public class PointException extends Exception {

    public PointException(String message) {
        super(message);
    }
}
