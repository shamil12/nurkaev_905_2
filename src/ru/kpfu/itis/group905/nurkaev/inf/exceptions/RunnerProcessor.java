/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

import java.io.IOException;

public class RunnerProcessor {

    public static void main(String[] args) throws IOException {
        final IBrokenRunner runner = new IBrokenRunner();
        execute(runner);
    }

    public static int execute(final IBrokenRunner runner) throws IOException {
        try {
            try {
                return runner.run();
            } catch (IOException e) {
                throw new IOException(); // выбрасываем на более выскоий уровень (все остальные блоки catch игнорируются)
            } catch (Exception e) {
                return -1;
            } catch (Throwable e) {
                return 0;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static class IBrokenRunner extends Exception {
        public int run() throws IOException {
            throw new IOException();
        }
    }
}
