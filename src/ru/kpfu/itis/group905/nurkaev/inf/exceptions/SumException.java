/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

public class SumException extends Exception {
    public SumException(String message) {
        super(message);
    }
}
