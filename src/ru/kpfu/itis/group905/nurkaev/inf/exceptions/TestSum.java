/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

public class TestSum {

    public static void main(String[] args) {
        TestSum a = new TestSum();
        a.test(100, 1);
    }

    public void test(int a, int b) {
        try {
            if (a + b > 100) {
                throw new SumException("");
            }
        } catch (SumException e) {
            System.out.println("The number is greater than 100.");
        }
    }
}
