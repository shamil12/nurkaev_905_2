/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.exceptions;

public class ThrowsExceptions {

    public static void methodThrowsClassCastException() {
        Object o = Integer.valueOf(9);
        String s = (String) o;
    }

    public static void main(String[] args) {
        try {
            ThrowsExceptions.methodThrowsClassCastException();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            ThrowsExceptions.methodThrowsNullPointerException();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void methodThrowsNullPointerException() {
        Object o = null;
        o.equals("s");
    }
}
