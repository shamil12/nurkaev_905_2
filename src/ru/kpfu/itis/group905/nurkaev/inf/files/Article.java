/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.files;

import java.io.Serializable;

public class Article implements Serializable {
    private final String url;
    private final String title;
    private final String summari;

    public Article(String url, String sammari, String title) {
        this.url = url;
        this.title = title;
        this.summari = sammari;
    }

    @Override
    public String toString() {
        return String.format("%s\n%s\n%s", url, title, summari);
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getSummari() {
        return summari;
    }
}
