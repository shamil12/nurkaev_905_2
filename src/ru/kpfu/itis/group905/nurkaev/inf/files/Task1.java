/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.files;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class Task1 {
    public static void main(String[] args) {
        String path = "src/ru/kpfu/itis/group905/nurkaev";
        File file = new File(path);


        ArrayList<File> fileNames = getFilesFromDirs(
                new ArrayList<>(), Objects.requireNonNull(file.listFiles())); // список с именами всех java файлов
        fileNames.sort(Comparator.comparingLong(File::lastModified)); // сортировка списка имён по дате создания

        for (File f : fileNames) {
            System.out.println(f.getName() + " " + f.lastModified());
        }
        System.out.println("\nCount: " + fileNames.size());
    }

    private static ArrayList<File> getFilesFromDirs(ArrayList<File> fileNames, File[] folders) {
        for (File folder : folders) {
            if (folder.isFile() && (folder + "").endsWith(".java")) {
                fileNames.add(folder);
            } else if (folder.isDirectory()) {
                fileNames = getFilesFromDirs(fileNames, Objects.requireNonNull(folder.listFiles()));
            }
        }

        return fileNames;
    }
}