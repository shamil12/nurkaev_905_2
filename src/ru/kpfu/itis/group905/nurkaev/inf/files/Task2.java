/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.files;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2 {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://habr.com/ru/");
        String htmlString;
        InputStream urlInputStream = url.openStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        StringBuilder article = new StringBuilder();
        Pattern pattern = Pattern.compile(
                ".*<h2 class=\"post__title\">" +
                "<a href=\"([\\w:/.\\- ]*)\" class=\"post__title_link\">(.*)" +
                "</a></h2>.*<div class=\"post__text post__text-html[ ]{2}post__text_v1 \">" +
                "(.*)</div>.*<a class=\"btn btn_x-large btn_outline_blue " +
                "post__habracut-btn\" href=\".*\">.*");

        int c;
        ArrayList<Article> articles = new ArrayList<>();
        while ((c = urlInputStream.read()) != -1) {
            if (c != '\n') {
                byteArrayOutputStream.write(c);
            } else {
                htmlString = byteArrayOutputStream.toString(StandardCharsets.UTF_8).trim();
                byteArrayOutputStream.reset();
                if (htmlString.equals("<article class=\"post post_preview\" lang=\"ru\">")) {
                    while (!htmlString.equals("</article>")) {
                        c = urlInputStream.read();
                        if (c != '\n') {
                            byteArrayOutputStream.write(c);
                        } else {
                            htmlString = byteArrayOutputStream.toString(StandardCharsets.UTF_8).trim();
                            article.append(htmlString);
                            byteArrayOutputStream.reset();
                        }
                    }

                    Matcher matcher = pattern.matcher(article.toString());
                    if (matcher.matches()) {
                        Article Article = new Article(
                                matcher.group(1),
                                matcher.group(3),
                                matcher.group(2));
                        articles.add(Article);
                    }

                    article = new StringBuilder();
                }
            }
        }

        try (ObjectOutputStream outputStream = new ObjectOutputStream(
                new FileOutputStream("src/ru/kpfu/itis/group905/nurkaev/inf/files/out.txt"))) {
            outputStream.writeObject(articles);
        }

        try (ObjectInputStream inputStream = new ObjectInputStream(
                new FileInputStream("src/ru/kpfu/itis/group905/nurkaev/inf/files/out.txt"))) {
            ArrayList<Article> arrayList = (ArrayList<Article>) inputStream.readObject();

            for (Article ar : arrayList) {
                System.out.println(
                        ar.getUrl() + "\n" +
                        ar.getTitle() + "\n" +
                        ar.getSummari() + "\n");
            }
        } catch (ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
        }

        urlInputStream.close();
        byteArrayOutputStream.close();
    }
}