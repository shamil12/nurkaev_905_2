/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.generics;

public class Account<T1, T2 extends Number> implements Accountable {
    private T1 id;
    private T2 sum;

    Account(T1 id, T2 sum) {
        this.id = id;
        this.sum = sum;
    }

    public T1 getId() {
        return id;
    }

    public T2 getSum() {
        return sum;
    }

    @Override
    public void setSum(int sum) {
        this.sum = (T2) ((Number) sum);
    }


    public void addToSum(T2 sum) {
        this.sum = (T2) ((Number) (this.sum.doubleValue() + sum.doubleValue()));
    }

    public void subtractToSum(T2 sum) {
        this.sum = (T2) ((Number) (this.sum.doubleValue() - sum.doubleValue()));
    }

    public String toString() {
        return "Id: " + getId() + ", Sum: " + getSum();
    }
}
