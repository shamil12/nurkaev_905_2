/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.generics;

public interface Accountable<T1, T2> {
    T1 getId();

    T2 getSum();

    void setSum(int sum);
}
