/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.generics;

import java.util.Arrays;

public class Array<T extends Accountable> {

    private T[] a;
    private int index;

    public Array(T[] a, int index) {
        this.a = a;
        this.index = index;
    }

    // adding x to the end of array
    public void add(T x) {
        if (index < a.length) {
            a[index] = x;
            index++;
        }
    }

    public int getIndex() {
        return index;
    }

    // adding x on position k. Be sure that position k is applicable to actual size of array.
    public void add(int k, T x) {
        if (k >= 0 && k < index && index != a.length) {
            for (int i = index - 1; k <= i; i--) {
                a[i + 1] = a[i];
            }
            a[k] = x;
            index++;
        }
    }

    // clearing the array
    public void clear() {
        index = 0;
    }

    // check if x is in array
    public boolean contains(T x) {
        for (int i = 0; i < index; i++) {
            if (a[i].getId().equals(x.getId())
                && a[i].getSum().equals(x.getSum())) return true;
        }
        return false;
    }

    // remove an element from position k
    public void remove(int k) {
        while (k < index - 1) {
            a[k] = a[k + 1];
            k++;
        }
        index--;
    }

    // remove first occurence of x in array
    public void delete(T x) {
        for (int i = 0; i < index; i++) {
            if (a[i].getId().equals(x.getId())
                    && a[i].getSum().equals(x.getSum())) {
                while (i < index - 1) {
                    a[i] = a[i + 1];
                    i++;
                }
                index--;
            }
        }
    }

    // convert array to string
    public String toString() {
        return Arrays.toString(Arrays.copyOf(a, index));
    }

}
