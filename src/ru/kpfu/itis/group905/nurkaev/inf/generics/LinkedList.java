/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.generics;

import java.util.Arrays;

public class LinkedList<T> {
    private Node head;
    private Node tail;
    private int size;

    public LinkedList(T[] a) {
        for (int i = 0; i < a.length; i++) {
            this.add(a[i]);
        }
    }

    public int size() {

        size = 0;
        for (Node curr = head; curr != null; curr = curr.next) {
            size++;
        }
        return size;
    }

    public void add(T m) {
        Node curr = new Node();
        curr.data = m;
        if (head == null) {
            head = curr;
        } else {
            tail.next = curr;
            curr.prev = tail;
        }
        tail = curr;
    }

    public void removeHead() {
        if (head.next == null) {
            head = null;
        } else {
            head = head.next;
            head.prev = null;
        }
    }

    public void removeTail() {
        if (head != null) {
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                tail = tail.prev;
                tail.next = null;
            }
        }
    }

    public void remove(int index) {
        if (head != null && index >= 1 && index <= size) {
            Node curr = head;
            if (index == 1) {
                this.removeHead();
            } else if (index == size) {
                this.removeTail();
            } else {
                for (int i = 1; i < index; i++) {
                    curr = curr.next;
                }
                curr.prev.next = curr.next;
            }
        } else throw new IndexOutOfBoundsException();
    }

    public void removeFirst(T k) {
        if (head.data == k) {
            this.removeHead();
        } else if (head != null) {
            Node curr = head;
            boolean contains = false;

            while (curr.data != k) {
                curr = curr.next;
                contains = true;
            }
            curr.prev.next = curr.next;

            if (!contains) {
                System.out.println("This item is not.");
            }
        }
    }

    public void removeAll(T k) {
        if (head != null) {
            Node curr = head;
            if (head.data == k) {
                this.removeHead();
            }
            curr = curr.next;
            boolean contains = false;

            while (curr != null) {
                if (curr.data == k) {
                    curr.prev.next = curr.next;
                    contains = true;
                }
                curr = curr.next;
            }

            if (!contains) {
                System.out.println("This item is not.");
            }
        }
    }

    public void insert(T m, T k) {
        if (head != null) {
            this.insertLeft(m, k);
            this.insertRight(m, k);
        }
    }

    public void insertLeft(T m, T k) {
        Node curr = head;
        while (curr.data != k) {
            curr = curr.next;
        }
        Node e = new Node();
        e.data = m;
        e.prev = curr.prev;
        e.next = curr;
        curr.prev.next = e;
        curr.prev = e;
    }

    public void insertRight(T m, T k) {
        Node curr = head;
        while (curr.data != k) {
            curr = curr.next;
        }
        Node e = new Node();
        e.data = m;
        e.prev = curr;
        e.next = curr.next;
        curr.next = e;
        curr.next.prev = e;
    }

    public String toString() {
        Object[] a = new Object[size()];
        if (head != null) {
            Node curr = head;
            for (int i = 0; curr != null; i++, curr = curr.next) {
                a[i] = curr.data;
            }
        } else {
            return null;
        }
        return Arrays.toString(a);
    }
}
