/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.generics;

public class Node<T> {
    public T data;
    public Node next;
    public Node prev;
}
