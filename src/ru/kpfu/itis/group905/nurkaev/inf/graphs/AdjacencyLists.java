/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.graphs;

import java.util.ArrayList;
import java.util.Arrays;

public class AdjacencyLists {

    ArrayList<ArrayList<Integer>> G;

    public AdjacencyLists(ArrayList<ArrayList<Integer>> adjacencyL) {
        G = adjacencyL;
    }

    public static AdjacencyMatrix toAdjacencyMatrix(AdjacencyLists AL) {
        int vertexCounts = AL.G.size();
        int[][] A = new int[vertexCounts][vertexCounts];

        for (int i = 0; i < vertexCounts; i++) {
            for (int j = 0; j < AL.G.get(i).size(); j++) {
                A[i][AL.G.get(i).get(j)]++;

                // если вершина имеет петлю, прибавить ещё 1, так как такое ребро считается дважды
                if (i == AL.G.get(i).get(j)) {
                    A[i][AL.G.get(i).get(j)]++;
                }
            }
        }

        return new AdjacencyMatrix(A);
    }

    public static IncidenceMatrix toIncidenceMatrix(AdjacencyLists AL) {
        int vertexCounts = AL.G.size();
        int edgeCounts = edgeCounts(AL);
        int[][] IM = new int[vertexCounts][edgeCounts];

        int e = 0;
        for (int i = 0; i < vertexCounts; i++) {
            for (int j = 0; j < AL.G.size(); j++) {

                // пропускаем ранее добавленные рёбра
                while (j < AL.G.get(i).size() && AL.G.get(i).get(j) < i) j++;
                // все рёбра уже добавлены ранее
                if (j >= AL.G.get(i).size()) break;

                if (i == AL.G.get(i).get(j)) {
                    IM[i][e] += 2;
                } else {
                    IM[i][e] += 1;
                    IM[AL.G.get(i).get(j)][e] += 1;
                }
                e++; // переходим к следующему столбцу
            }
        }

        return new IncidenceMatrix(IM);
    }

    private static int edgeCounts(AdjacencyLists AL) {
        int edgeCounts = 0;
        for (int i = 0; i < AL.G.size(); i++) {
            for (int j = 0; j < AL.G.get(i).size(); j++) {
                while (j < AL.G.get(i).size() && AL.G.get(i).get(j) < i) j++;
                if (j >= AL.G.get(i).size()) break;
                edgeCounts++;
            }
        }
        return edgeCounts;
    }

    public static EdgesVertexesLists toEdgesVertexesLists(AdjacencyLists AL) {
        ArrayList<ArrayList<Integer>> EVL = new ArrayList<>();

        for (int i = 0; i < AL.G.size(); i++) {
            for (int j = 0; j < AL.G.get(i).size(); j++) {

                // пропускаем ранее добавленные рёбра
                while (j < AL.G.get(i).size() && AL.G.get(i).get(j) < i) j++;
                // все рёбра уже добавлены ранее
                if (j >= AL.G.get(i).size()) break;

                EVL.add(new ArrayList<>(Arrays.asList(i, AL.G.get(i).get(j))));
            }
        }

        return new EdgesVertexesLists(EVL);
    }

    @Override
    public String toString() {
        StringBuilder graph = new StringBuilder();
        for (int i = 0; i < G.size(); i++) {
            graph.append(i + ": ");
            for (Integer vertex : G.get(i)) {
                graph.append(vertex + "  ");
            }
            graph.append("\n");
        }
        return graph.toString();
    }
}