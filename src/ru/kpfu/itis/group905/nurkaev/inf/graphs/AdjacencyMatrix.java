/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.graphs;

import java.util.ArrayList;
import java.util.Arrays;

public class AdjacencyMatrix {

    int[][] A;

    public AdjacencyMatrix(int[][] adjacencyMatrix) {
        A = adjacencyMatrix;
    }

    public static IncidenceMatrix toIncidenceMatrix(AdjacencyMatrix AM) {
        int vertexCounts = AM.A.length;
        int edgeCounts = edgeCounts(AM);
        int[][] IM = new int[vertexCounts][edgeCounts];

        int e = 0;
        for (int i = 0; i < vertexCounts; i++) {
            for (int j = i; j < vertexCounts; j++) {
                if (AM.A[i][j] != 0) {
                    if (i == j) {
                        IM[i][e] += 2;
                        e++; // переходим к следующему столбцу
                    } else {
                        for (int k = 0; k < AM.A[i][j]; k++) {
                            IM[i][e] += 1;
                            IM[j][e] += 1;
                            e++; // переходим к следующему столбцу
                        }
                    }
                }
            }
        }

        return new IncidenceMatrix(IM);
    }

    public static AdjacencyLists toAdjacencyLists(AdjacencyMatrix AM) {
        int vertexCounts = AM.A.length;
        ArrayList<ArrayList<Integer>> AL = new ArrayList<>();

        for (int i = 0; i < vertexCounts; i++) {
            ArrayList<Integer> vertexAdjacency = new ArrayList<>();
            for (int j = 0; j < vertexCounts; j++) {
                // Добавляем рёбра в список смежности с учётом их кратности.
                // Также учитываем, что вершина может являться петлёй.
                if (i == j) {
                    for (int k = 0; k < (AM.A[i][j] / 2); k++) {
                        vertexAdjacency.add(j);
                    }
                } else
                    for (int k = 0; k < AM.A[i][j]; k++) {
                        vertexAdjacency.add(j);
                    }
            }
            AL.add(vertexAdjacency);
        }

        return new AdjacencyLists(AL);
    }

    private static int edgeCounts(AdjacencyMatrix AM) {
        int edgeCounts = 0;

        for (int i = 0; i < AM.A.length; i++) {
            for (int j = i; j < AM.A.length; j++) {
                // каждое ребро, являющееся петлёй, считалась дважды
                if (i == j) {
                    edgeCounts += AM.A[i][j] / 2;
                } else {
                    edgeCounts += AM.A[i][j];
                }
            }
        }

        return edgeCounts;
    }

    public static EdgesVertexesLists toEdgesVertexesLists(AdjacencyMatrix AM) {
        int vertexCounts = AM.A.length;
        ArrayList<ArrayList<Integer>> EVL = new ArrayList<>();

        for (int i = 0; i < vertexCounts; i++)
            for (int j = i; j < vertexCounts; j++) {
                // Добавляем ребра в список вершин с учётом их кратности
                // Также учитываем, что вершина может являться петлёй.
                if (AM.A[i][j] != 0) {
                    if (i == j) {
                        EVL.add(new ArrayList<>(Arrays.asList(i, i)));
                    } else {
                        for (int k = 0; k < AM.A[i][j]; k++) {
                            EVL.add(new ArrayList<>(Arrays.asList(i, j)));
                        }
                    }
                }
            }

        return new EdgesVertexesLists(EVL);
    }

    @Override
    public String toString() {
        StringBuilder graph = new StringBuilder();
        for (int[] ints : A) {
            for (int isAdjacency : ints) {
                graph.append(isAdjacency + "  ");
            }
            graph.append("\n");
        }
        return graph.toString();
    }
}