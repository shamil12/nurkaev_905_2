/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.graphs;

import java.util.ArrayList;

public class EdgesVertexesLists {

    ArrayList<ArrayList<Integer>> A;

    public EdgesVertexesLists(ArrayList<ArrayList<Integer>> edgesVertexesLists) {
        A = edgesVertexesLists;
    }

    public static AdjacencyMatrix toAdjacencyMatrix(EdgesVertexesLists EVL) {
        int vertexCounts = EVL.A.get(EVL.A.size() - 1).get(1) + 1;
        int edgeCounts = EVL.A.size();
        int[][] AM = new int[vertexCounts][vertexCounts];

        // создаём верхнетреугольную матрицу смежности,
        // одновременно симметрично отражая её относительно главной диагонали
        for (int i = 0; i < edgeCounts; i++) {
            AM[EVL.A.get(i).get(0)][EVL.A.get(i).get(1)] += 1;
            AM[EVL.A.get(i).get(1)][EVL.A.get(i).get(0)] += 1;
        }

        return new AdjacencyMatrix(AM);
    }

    public static IncidenceMatrix toIncidenceMatrix(EdgesVertexesLists EVL) {
        int vertexCounts = EVL.A.get(EVL.A.size() - 1).get(1) + 1;
        int edgeCounts = EVL.A.size();
        int[][] IM = new int[vertexCounts][edgeCounts];

        int e = 0;
        for (int i = 0; i < edgeCounts; i++) {
            // если ребро является петлёй
            if (EVL.A.get(i).get(0) == EVL.A.get(i).get(1)) {
                IM[EVL.A.get(i).get(0)][e] += 2;
            } else {
                IM[EVL.A.get(i).get(0)][e] += 1;
                IM[EVL.A.get(i).get(1)][e] += 1;
            }
            e++;
        }

        return new IncidenceMatrix(IM);
    }

    public static AdjacencyLists toAdjacencyLists(EdgesVertexesLists EVL) {
        int vertexCounts = EVL.A.get(EVL.A.size() - 1).get(1) + 1;
        int edgeCounts = EVL.A.size();
        ArrayList<ArrayList<Integer>> AL = new ArrayList<>();
        // резервируем память под список с vertexCounts списками
        for (int i = 0; i < vertexCounts; i++) {
            AL.add(new ArrayList<>());
        }

        for (int i = 0; i < edgeCounts; i++) {
            // если ребро является петлёй
            if (EVL.A.get(i).get(0) == EVL.A.get(i).get(1)) {
                AL.get(EVL.A.get(i).get(0)).add(EVL.A.get(i).get(0));
            } else {
                AL.get(EVL.A.get(i).get(0)).add(EVL.A.get(i).get(1));
                AL.get(EVL.A.get(i).get(1)).add(EVL.A.get(i).get(0));
            }
        }

        return new AdjacencyLists(AL);
    }

    @Override
    public String toString() {
        StringBuilder graph = new StringBuilder();
        for (int i = 0; i < A.size(); i++) {
            graph.append(i + 1 + ": " + A.get(i).get(0) +
                    "  " + A.get(i).get(1) + "\n");
        }
        return graph.toString();
    }
}