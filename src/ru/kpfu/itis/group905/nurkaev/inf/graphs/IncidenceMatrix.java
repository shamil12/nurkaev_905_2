/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.graphs;

import java.util.ArrayList;
import java.util.Arrays;

public class IncidenceMatrix {

    int[][] A;

    public IncidenceMatrix(int[][] incidenceMatrix) {
        A = incidenceMatrix;
    }

    public static AdjacencyMatrix toAdjacencyMatrix(IncidenceMatrix IM) {
        int vertexCounts = IM.A.length;
        int edgeCounts = IM.A[0].length;
        int[][] AM = new int[vertexCounts][vertexCounts];

        // создаём верхнетреугольную матрицу смежности,
        // одновременно симметрично отражая её относительно главной диагонали
        for (int j = 0; j < edgeCounts; j++) {
            for (int i = 0; i < vertexCounts; i++) {
                // если ребро является петлёй
                if (IM.A[i][j] == 2) {
                    AM[i][i] += 2; // += чтобы захватить случаи, когда петель несколько
                    break;
                } else if (IM.A[i][j] == 1) {
                    // ищем другую вершину с единичкой (вместе они являются концами ребра)
                    for (int k = i + 1; k < vertexCounts; k++) {
                        if (IM.A[k][j] == 1) {
                            AM[i][k] += 1;
                            AM[k][i] += 1;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        return new AdjacencyMatrix(AM);
    }

    public static AdjacencyLists toAdjacencyLists(IncidenceMatrix IM) {
        int vertexCounts = IM.A.length;
        int edgeCounts = IM.A[0].length;
        ArrayList<ArrayList<Integer>> AL = new ArrayList<>();
        // резервируем память под список с vertexCounts списками
        for (int i = 0; i < vertexCounts; i++) {
            AL.add(new ArrayList<>());
        }

        for (int j = 0; j < edgeCounts; j++) {
            for (int i = 0; i < vertexCounts; i++) {
                // если ребро является петлёй
                if (IM.A[i][j] == 2) {
                    AL.get(i).add(i);
                    break;
                } else if (IM.A[i][j] == 1) {
                    // ищем другую вершину с единичкой (вместе они являются концами ребра)
                    for (int k = i + 1; k < vertexCounts; k++) {
                        if (IM.A[k][j] == 1) {
                            AL.get(i).add(k);
                            AL.get(k).add(i);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        return new AdjacencyLists(AL);
    }

    public static EdgesVertexesLists toEdgesVertexesLists(IncidenceMatrix IM) {
        int vertexCounts = IM.A.length;
        int edgeCounts = IM.A[0].length;
        ArrayList<ArrayList<Integer>> EVL = new ArrayList<>();

        for (int j = 0; j < edgeCounts; j++) {
            for (int i = 0; i < vertexCounts; i++) {
                // если ребро является петлёй
                if (IM.A[i][j] == 2) {
                    EVL.add(new ArrayList<>(Arrays.asList(i, i)));
                    break;
                }
                else if (IM.A[i][j] == 1) {
                    // ищем другую вершину с единичкой (вместе они являются концами ребра)
                    for (int k = i + 1; k < vertexCounts; k++) {
                        if (IM.A[k][j] == 1) {
                            EVL.add(new ArrayList<>(Arrays.asList(i, k)));
                            break;
                        }
                    }
                    break;
                }
            }
        }

        return new EdgesVertexesLists(EVL);
    }

    @Override
    public String toString() {
        StringBuilder graph = new StringBuilder();
        for (int[] ints : A) {
            for (int isIncidence : ints) {
                graph.append(isIncidence + "  ");
            }
            graph.append("\n");
        }
        return graph.toString();
    }
}