/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.graphs;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    // Матрица создается размером n*n, так как используется нумерация с НУЛЯ

    public static void main(String[] args) {
        int[][] adjacencyM = {
                {0, 2, 1, 0, 0},
                {2, 0, 1, 1, 0},
                {1, 1, 2, 0, 0}, // вершина номер 2 имеет петлю
                {0, 1, 0, 0, 0},
                {0, 0, 0, 0, 2}}; // вершина номер 4 изолирована и имеет петлю

        int[][] incidenceM = {
                {1, 1, 1, 0, 0, 0, 0},
                {1, 1, 0, 1, 1, 0, 0},
                {0, 0, 1, 1, 0, 2, 0}, // вершина номер 2 имеет петлю
                {0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 2}}; // вершина номер 4 изолирована и имеет петлю

        ArrayList<ArrayList<Integer>> adjacencyL = new ArrayList<>(5);
        adjacencyL.add(new ArrayList<>(Arrays.asList(1, 1, 2)));
        adjacencyL.add(new ArrayList<>(Arrays.asList(0, 0, 2, 3)));
        adjacencyL.add(new ArrayList<>(Arrays.asList(0, 1, 2)));
        adjacencyL.add(new ArrayList<>(Arrays.asList(1)));
        adjacencyL.add(new ArrayList<>(Arrays.asList(4)));

        ArrayList<ArrayList<Integer>> edgesVertexesL = new ArrayList<>(7);
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(0, 1)));
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(0, 1)));
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(0, 2)));
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(1, 2)));
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(1, 3)));
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(2, 2)));
        edgesVertexesL.add(new ArrayList<>(Arrays.asList(4, 4)));

        AdjacencyMatrix adjacencyMatrix = new AdjacencyMatrix(adjacencyM);
        IncidenceMatrix incidenceMatrix = new IncidenceMatrix(incidenceM);
        AdjacencyLists adjacencyLists = new AdjacencyLists(adjacencyL);
        EdgesVertexesLists edgesVertexesLists = new EdgesVertexesLists(edgesVertexesL);

        System.out.println("Adjacency matrix:\n" + adjacencyMatrix);
        System.out.println("Incidence matrix:\n" + incidenceMatrix);
        System.out.println("Adjacency lists:\n" + adjacencyLists);
        System.out.println("Lists of edges and vertexes:\n" + edgesVertexesLists);

        ///////////////////////////////////TEST////////////////////////////////////////////////

        System.out.println(AdjacencyMatrix.toIncidenceMatrix(adjacencyMatrix));
        System.out.println(AdjacencyMatrix.toAdjacencyLists(adjacencyMatrix));
        System.out.println(AdjacencyMatrix.toEdgesVertexesLists(adjacencyMatrix));
        System.out.println(IncidenceMatrix.toAdjacencyMatrix(incidenceMatrix));
        System.out.println(IncidenceMatrix.toAdjacencyLists(incidenceMatrix));
        System.out.println(IncidenceMatrix.toEdgesVertexesLists(incidenceMatrix));
        System.out.println(AdjacencyLists.toAdjacencyMatrix(adjacencyLists));
        System.out.println(AdjacencyLists.toIncidenceMatrix(adjacencyLists));
        System.out.println(AdjacencyLists.toEdgesVertexesLists(adjacencyLists));
        System.out.println(EdgesVertexesLists.toAdjacencyMatrix(edgesVertexesLists));
        System.out.println(EdgesVertexesLists.toIncidenceMatrix(edgesVertexesLists));
        System.out.println(EdgesVertexesLists.toAdjacencyLists(edgesVertexesLists));
    }
}