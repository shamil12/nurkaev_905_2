/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.iterator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class LinkedList implements Iterable<Integer> {
    private Node head;
    private Node tail;
    private int size;

    public LinkedList() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                add(scanner.nextInt());
            } catch (Exception e) {
                break;
            }
        }
    }

    public int size() {
        for (Node curr = head; curr != null; curr = curr.next) {
            size++;
        }
        return size;
    }

    public void add(int m) {
        Node curr = new Node();
        curr.data = m;
        if (head == null) {
            head = curr;
        } else {
            tail.next = curr;
            curr.prev = tail;
        }
        tail = curr;
    }

    public int max() {
        int max = head.data;
        if (head != null) {
            for (Node curr = head; curr != null; curr = curr.next) {
                if (curr.data > max) {
                    max = curr.data;
                }
            }
        }
        return max;
    }

    public int sum() {
        int sum = head.data;
        if (head != null) {
            for (Node curr = head; curr != null; curr = curr.next) {
                sum += curr.data;
            }
        }
        return sum;
    }

    public boolean hasNegative() {
        for (Node curr = head; curr != null; curr = curr.next) {
            if (curr.data < 0) {
                return true;
            }
        }
        return false;
    }

    public void removeHead() {
        if (head.next == null) {
            head = null;
        } else {
            head = head.next;
            head.prev = null;
        }
    }

    public void removeTail() {
        if (head != null) {
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                tail = tail.prev;
                tail.next = null;
            }
        }
    }

    public void remove(int index) {
        if (head != null && index >= 1 && index <= size) {
            Node curr = head;
            if (index == 1) {
                this.removeHead();
            } else if (index == size) {
                this.removeTail();
            } else {
                for (int i = 1; i < index; i++) {
                    curr = curr.next;
                }
                curr.prev.next = curr.next;
            }
        } else throw new IndexOutOfBoundsException();
    }

    public void removeFirst(int k) {
        if (head.data == k) {
            this.removeHead();
        } else if (head != null) {
            Node curr = head;
            boolean contains = false;

            while (curr.data != k) {
                curr = curr.next;
                contains = true;
            }
            curr.prev.next = curr.next;

            if (!contains) {
                System.out.println("Такого элемента нет");
            }
        }
    }

    public void removeAll(int k) {
        if (head != null) {
            Node curr = head;
            if (head.data == k) {
                this.removeHead();
            }
            curr = curr.next;
            boolean contains = false;

            while (curr != null) {
                if (curr.data == k) {
                    curr.prev.next = curr.next;
                    contains = true;
                }
                curr = curr.next;
            }

            if (!contains) {
                System.out.println("Такого элемента нет");
            }
        }
    }

    public void insert(int m, int k) {
        if (head != null) {
            this.insertLeft(m, k);
            this.insertRight(m, k);
        }
    }

    public void insertLeft(int m, int k) {
        Node curr = head;
        while (curr.data != k) {
            curr = curr.next;
        }
        Node e = new Node();
        e.data = m;
        e.prev = curr.prev;
        e.next = curr;
        curr.prev.next = e;
        curr.prev = e;
    }

    public void insertRight(int m, int k) {
        Node curr = head;
        while (curr.data != k) {
            curr = curr.next;
        }
        Node e = new Node();
        e.data = m;
        e.prev = curr;
        e.next = curr.next;
        curr.next = e;
        curr.next.prev = e;
    }

    public String toString() {
        int[] a = new int[size()];
        if (head != null) {
            Node curr = head;
            for (int i = 0; curr != null; i++, curr = curr.next) {
                a[i] = curr.data;
            }
        } else {
            return null;
        }
        return Arrays.toString(a);
    }

    @Override
    public Iterator<Integer> iterator() {
        return new EvenPositionIterator(this.head);
    }

    private class EvenPositionIterator implements Iterator<Integer> {

        private Node curr;

        public EvenPositionIterator(Node head) {
            curr = head;
        }

        @Override
        public boolean hasNext() {
            return curr != null;
        }

        @Override
        public Integer next() {
            int a = curr.data;
            if (curr.next != null) curr = curr.next.next;
            else curr = null;
            return a;
        }

    }

    private class Node {
        public int data;
        public Node next;
        public Node prev;
    }
}



