/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.iterator;

import java.util.Arrays;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {
        LinkedList list1 = new LinkedList();

        for (int i : list1) {
            System.out.print(i + " ");
        }

        System.out.println("\n===============================");

        MyLinkedList<Integer> list2 = new MyLinkedList<>(new Integer[]{1, 2, 3, 4, 5});
        ListIterator it = list2.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " ");
        }

        System.out.println("\n===============================");

        while (it.hasPrevious()) {
            System.out.print(it.previous() + " ");
        }

        System.out.println("\n===============================");

        java.util.LinkedList list3 = new java.util.LinkedList();
        list3.addAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
        ListIterator listIterator = list3.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.remove();
        System.out.println(list3);
        listIterator.previous();
        listIterator.set(33);
        System.out.println(list3);
        listIterator.add(22);
        System.out.println(list3);
    }
}
