/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.iterator;

import java.util.ListIterator;

public class MyLinkedList<T> implements Iterable<T> {
    public Node head;
    public Node tail;
    public int size;

    public MyLinkedList(T[] a) {
        for (int i = 0; i < a.length; i++) {
            this.add(a[i]);
        }
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        Node curr = head;
        for (int i = 0; i < index; i++) {
            curr = curr.next;
        }
        return (T) curr.data;
    }

    public void add(T m) {
        Node curr = new Node();
        curr.data = m;
        if (head == null) {
            head = curr;
        } else {
            tail.next = curr;
            curr.prev = tail;
        }
        tail = curr;
        size += 1;
    }

    public boolean contains(T m) {
        Node curr = head;
        for (int i = 0; i < size; i++) {
            if (curr.data == m) return true;
            curr = curr.next;
        }
        return false;
    }

    public void removeHead() {
        if (head.next == null) {
            head = null;
        } else {
            head = head.next;
            head.prev = null;
        }
        size -= 1;
    }

    public void removeTail() {
        if (head != null) {
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                tail = tail.prev;
                tail.next = null;
            }
            size -= 1;
        }
    }

    // удаление элемента по ИНДЕКСУ
    public void remove(int index) {
        if (head != null && index >= 0 && index < size) {
            Node curr = head;
            if (index == 0) {
                this.removeHead();
            } else if (index == size - 1) {
                this.removeTail();
            } else {
                for (int i = 0; i < index; i++) {
                    curr = curr.next;
                }
                curr.prev.next = curr.next;
                curr.next.prev = curr.prev;
                size -= 1;
            }
        } else throw new IndexOutOfBoundsException();
    }

    // удаление переданного элемента
    public void remove(T k) {
        if (head.data == k) {
            this.removeHead();
        } else if (tail.data == k) {
            this.removeTail();
        } else if (head != null) {
            Node curr = head;
            while (curr.data != k) {
                curr = curr.next;
            }
            curr.prev.next = curr.next;
            curr.next.prev = curr.prev;
            size -= 1;
        }
    }

    public void insertLeft(T m, T k) {
        Node e = new Node();
        e.data = m;
        if (head.data == k) {
            e.next = head;
            head.prev = e;
            head = e;
        } else {
            Node curr = head;
            while (curr.data != k) {
                curr = curr.next;
            }
            e.prev = curr.prev;
            e.next = curr;
            curr.prev.next = e;
            curr.prev = e;
        }
        size += 1;
    }

    @Override
    public ListIterator<T> iterator() {
        return new EvenPosListIterator(this.head, this.tail);
    }

    private class EvenPosListIterator implements ListIterator<T> {

        private Node curr1;
        private Node curr2;
        private int index1;
        private int index2;

        public EvenPosListIterator(Node head, Node tail) {
            curr1 = head;
            curr2 = tail;
            index1 = -2;
            index2 = size + 1;
        }

        @Override
        public boolean hasNext() {
            return curr1 != null;
        }

        @Override
        public T next() {
            T a = (T) curr1.data;
            if (curr1.next != null) curr1 = curr1.next.next;
            else curr1 = null;
            return a;
        }

        @Override
        public boolean hasPrevious() {
            return curr2 != null;
        }

        @Override
        public T previous() {
            T a = (T) curr2.data;
            if (curr2.prev != null) curr2 = curr2.prev.prev;
            else curr2 = null;
            return a;
        }

        @Override
        public int nextIndex() {
            return index1 += 2;
        }

        @Override
        public int previousIndex() {
            return index2 -= 2;
        }

        @Override
        public void remove() {
        }

        @Override
        public void set(T t) {
        }

        @Override
        public void add(T t) {
        }
    }

    private class Node<T> {
        public T data;
        public Node next;
        public Node prev;
    }
}
