/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * KR 1
 */

package ru.kpfu.itis.group905.nurkaev.inf.kr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/kr1/in.txt");
        Scanner scan = new Scanner(file);
        Stack<String> stack = new Stack<>();

        while (scan.hasNextLine()) {
            stack.push(scan.next());
        }
        scan.close();

        PrintWriter out = new PrintWriter("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/kr1/out.txt");
        Object[] a = stack.toArray();
        for (int i = a.length - 1; i >= 0; i--) {
            out.println(a[i]);
        }
        out.close();

        PrintWriter out2 = new PrintWriter("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/kr1/out2.txt");
//        Collections.sort(stack, new Comparable<>());
        out2.close();
    }
}
