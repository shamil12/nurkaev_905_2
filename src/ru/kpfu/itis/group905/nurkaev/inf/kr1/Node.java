/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * KR 1
 */

package ru.kpfu.itis.group905.nurkaev.inf.kr1;

public class Node<T> implements Comparable<T> {
    public T data;
    public Node next;

    @Override
    public int compareTo(T s) {
       String s1 = this.data.toString();
       String s2 = s.toString();
       if (s1.length() > s2.length()) return 1;
       if (s1.length() < s2.length()) return -1;
       return 0;
    }
}
