/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * KR 1
 */

package ru.kpfu.itis.group905.nurkaev.inf.kr1;

public class Stack<T> extends java.util.LinkedList<T> {
    public Node first;
    public Node last;
    public int size;

    public Stack() {
        size = 0;
    }

    public int size() {
        return size;
    }

    public void push(T m) {
        Node curr = new Node();
        curr.data = m;
        if (first == null) {
            first = curr;
        } else {
            last.next = curr;
        }
        last = curr;
        size += 1;
    }

    public T pop() {
        Node e = last;
        if (isEmpty()) try {
            throw new StackIsEmptyException();
        } catch (StackIsEmptyException ex) {
            ex.printStackTrace();
        }
        else {
            Node curr = last;
            for (int i = 0; i < size - 1; i++) {
                curr = curr.next;
            }
            e = last;
            last = curr;
            size -= 1;
        }
        return (T) e.data;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        Node curr = first;
        for (int i = 0; i < size; i++) {
            s.append(curr.data).append(" ");
            curr = curr.next;
        }
        return s.toString();
    }

    public Object[] toArray() {
        Object[] a = new Object[size];
        if (first != null) {
            Node curr = first;
            for (int i = 0; curr != null; i++) {
                a[i] = curr.data;
                curr = curr.next;
            }
        } else {
            return null;
        }
        return a;
    }

    public boolean isEmpty() {
        if (first == null) return true;
        return false;
    }

    public void clear() {
        first = null;
        size = 0;
    }
}
