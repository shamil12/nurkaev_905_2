/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 1
 */

package ru.kpfu.itis.group905.nurkaev.inf.maps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/maps/input.txt");
        Scanner scan = new Scanner(file);
        HashMap<Integer, Integer> numbers = new HashMap<>();

        while (scan.hasNext()) {
            int key = scan.nextInt();
            if (numbers.get(key) == null) {
                numbers.put(key, 0);
            }
            if (numbers.containsKey(key)) {
                numbers.put(key, numbers.get(key) + 1);
            }
        }
        scan.close();

        for (Map.Entry entry : numbers.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}