/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework Task 2
 */

package ru.kpfu.itis.group905.nurkaev.inf.maps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Words {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("C:/Users/nurka/Desktop/Projects/nurkaev_905_2/src/ru/kpfu/itis/group905/nurkaev/inf/maps/input2.txt");
        Scanner scan = new Scanner(file);
        HashMap<String, Integer> numbers = new HashMap<>();

        while (scan.hasNext()) {
            String key = scan.next().toLowerCase();
            if (numbers.get(key) == null) {
                numbers.put(key, 0);
            }
            if (numbers.containsKey(key)) {
                numbers.put(key, numbers.get(key) + 1);
            }
        }
        scan.close();

        for (Map.Entry entry : numbers.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}
