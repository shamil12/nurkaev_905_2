/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.myList;

/*
Write the implementation of methods marked with TODO and check them in main
Don't forget to sign out your code! (this sourse file)
*/

import java.util.Arrays;

public class Array {

    private int[] a;
    private int index;

    public Array() {
        a = new int[100];
        index = 0;
    }

    // adding x to the end of array
    public void add(int x) {
        if (index < a.length) {
            a[index] = x;
            index++;
        }
    }

    // adding x on position k. Be sure that position k is applicable to actual size of array.
    public void add(int k, int x) {
        if (k >= 0 && k < index && index != a.length) {
            int i = index - 1;
            while (i >= k) {
                a[i + 1] = a[i];
                i--;
            }
            a[k] = x;
            index++;
        }
    }

    // clearing the array
    public void clear() {
        index = 0;
    }

    // check if x is in array
    public boolean contains(int x) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) return true;
        }
        return false;
    }

    // remove an element from position k
    public void remove(int k) {
        while (k < a.length - 1) {
            a[k] = a[k + 1];
            k++;
        }
        index--;
    }

    // remove first occurence of x in array
    public void delete(int x) {
        int i = 0;
        while (a[i] != x) i++;
        while (i < a.length - 1) {
            a[i] = a[i + 1];
            i++;
        }
        index--;
    }

    // convert array to string
    public String toString() {
        return Arrays.toString(Arrays.copyOf(a, index));
    }

}
