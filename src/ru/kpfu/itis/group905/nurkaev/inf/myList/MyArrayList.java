/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.myList;

import java.util.Arrays;

public class MyArrayList implements MyList {
    private int[] a;
    private int index;

    MyArrayList(int size) {
        a = new int[size];
    }

    @Override
    public boolean add(int x) {
        if (index == a.length) {
            return false;
        }
        a[index] = x;
        index++;
        return true;
    }

    @Override
    public void add(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= index) {
            throw new MyIndexOutOfBoundException("k is >= real size of list or negative.");
        } else if (index == a.length) {
            throw new MyIndexOutOfBoundException("The array is already filled");
        } else {
            int i = index - 1;
            while (i >= k) {
                a[i + 1] = a[i];
                i--;
            }
            a[k] = x;
            index++;
        }
    }

    @Override
    public void set(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= index) {
            throw new MyIndexOutOfBoundException("k is >= real size of list or negative.");
        } else if (index == a.length) {
            throw new MyIndexOutOfBoundException("The array is already filled");
        } else {
            a[k] = x;
        }
    }

    @Override
    public boolean contains(int x) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) return true;
        }
        return false;
    }

    @Override
    public int add(int[] a) {
        int j = 0;
        for (int i = index; i < this.a.length && j < a.length; i++, j++) {
            this.a[i] = a[j];
            index++;
        }
        return j;
    }

    @Override
    public int contains(int[] a) {
        int k = 0;
        for (int i : this.a) {
            for (int j : a) {
                if (i == j) {
                    k++;
                    break;
                }
            }
        }
        return k;
    }

    public String toString() {
        return Arrays.toString(Arrays.copyOf(a, index));
    }
}
