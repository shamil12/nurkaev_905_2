/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.myList;

public class MyIndexOutOfBoundException extends Exception {
    MyIndexOutOfBoundException(String msg) {
        super(msg);
    }
}
