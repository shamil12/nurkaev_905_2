/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.myList;

import ru.kpfu.itis.group905.nurkaev.algorithms.pair1.Node;

import java.util.Arrays;

public class MyLinkedList implements MyList {
    private Node head;
    private Node tail;
    private int size;

    public MyLinkedList() {
        size = 0;
    }

    public MyLinkedList(int size) {
        this.size = size;
    }

    public int count() {
        int count = 0;
        for (Node curr = head; curr != null; curr = curr.next) {
            count++;
        }
        return count;
    }

    @Override
    public boolean add(int x) {
        if (count() != size) {
            Node curr = new Node();
            curr.data = x;
            if (head == null) {
                head = curr;
            } else {
                tail.next = curr;
                curr.prev = tail;
            }
            tail = curr;
            return true;
        }
        return false;
    }

    @Override
    public void add(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= size) {
            throw new MyIndexOutOfBoundException("k is >= real size of list or negative.");
        } else if (size == count()) {
            throw new MyIndexOutOfBoundException("The array is already filled.");
        } else if (k == 0) {
            Node e = new Node();
            e.data = x;
            head.prev = e;
            e.next = head;
            head = e;
        } else if (k == count()) {
            this.add(x);
        } else {
            Node curr = head;
            for (int i = 0; i < k; i++) {
                curr = curr.next;
            }
            Node e = new Node();
            e.data = x;
            e.prev = curr.prev;
            e.next = curr;
            curr.prev.next = e;
            curr.prev = e;
        }
    }

    @Override
    public void set(int k, int x) throws MyIndexOutOfBoundException {
        if (k < 0 || k >= size) {
            throw new MyIndexOutOfBoundException("k is >= real size of list or negative.");
        } else if (size == count()) {
            throw new MyIndexOutOfBoundException("The array is already filled.");
        } else {
            Node curr = head;
            for (int i = 0; i < k; i++) curr = curr.next;
            curr.data = x;
        }
    }

    @Override
    public boolean contains(int x) {
        for (Node curr = head; curr != null; curr = curr.next) {
            if (curr.data == x) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int add(int[] a) {
        int count = count();
        int j = 0;
        for (; count < size && j < a.length; j++, count++) {
            this.add(a[j]);
        }
        return j;
    }

    @Override
    public int contains(int[] a) {
        int k = 0;
        for (Node curr = head; curr != null; curr = curr.next) {
            for (int x : a) {
                if (curr.data == x) {
                    k++;
                    break;
                }
            }
        }
        return k;
    }

    public String toString() {
        int[] a = new int[count()];
        if (head != null) {
            Node curr = head;
            for (int i = 0; curr != null; i++, curr = curr.next) {
                a[i] = curr.data;
            }
        } else {
            return null;
        }
        return Arrays.toString(a);
    }
}
