/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.regex;

/*
Напишите регулярное выражения для даты и времени в промежутке
с 6 марта 1237 12:00 по 27 февраля 1978 21:35
в формате MM/DD/YYYY HH:MM
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task1 {
    public static void main(String[] args) {
        String s = "12/27/1977 21:35";

        String BA = "(0[1-9]|1[012])/(0[1-9]|[12]\\d|3[01])/(1[2-9]\\d\\d)\\s([01]\\d|2[0-3]):[0-5]\\d";

        String MB = "((((0[13578])|(1[0|2]))[/]((0[1-9])|([1|2][0-9])|(3[0|1])))|(((0[469])|(11))[/]((0[1-9])|([1|2][0-9])|(30)))";
        String YB = "/((1[3-8][02468][1235679])|(1[3-8][13579][01345789])|(12[468][1235679])|(12[579][01345789])|(123[89])|(19[0246][1235679])|(19[1357][013457]))";
        String time = "\\s(0\\d|1\\d|2[0-3]):[0-5]\\d";
        String February = "|(02[/](0[1-9]|[1|2][0-8])))";;

        String MBound2 = "((01/(0[1-9]|[12]\\d|3[01]))|(02/(0[1-9]|1\\d|2[0-7])))";
        String YBound2 = "/1978";
        String time2 = "((\\s(0\\d|1\\d|20):[0-5]\\d)|(\\s((21:[0-2]\\d)|(21:3[0-5]))))";
        String February2 = "|(02[/](0[1-9]|[1|2]\\d)))";

        String MBound3 = "(((0[578]|1[02])/(0[1-9]|[12]\\d|3[01]))|((0[469]|11)/(0[1-9]|[12]\\d|30))|(03[/](0[6-9]|[12]\\d|3[01])))";
        String YBound3 = "/1237";
        String time3 = "\\s(1[2-9]|2[0-3]):[0-5]\\d";

        String visokos = "/((1[3-8][02468][048])|(1[3-8][13579][26])|(12[468][048])|(12[579][26])|(19[0246][048])|(19[1357][26]))";

        Pattern pattern = Pattern.compile("(((" + MB + February + YB + ")|(" + MB + February2 + visokos + "))" + time + ")|" +
                "(" + MBound2 + YBound2 + time2 + ")|" +
                "(" + MBound3 + YBound3 + time3 + ")|" + BA);

        Matcher matcher = pattern.matcher(s);

        if (matcher.matches()) {
            System.out.println("Подходит: " + s);
        } else {
            System.out.println("Не подходит: " + s);
        }
    }
}