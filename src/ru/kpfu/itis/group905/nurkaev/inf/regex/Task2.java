/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.regex;

/*
Напишите регулярное выражение для вещественного числа с периодом.
Подходят: 0, -6, -0.5, +2, 0,0(64),
Не подходят: -0, 001, 0,(35)00, -3,750
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2 {
    public static void main(String[] args) {
        String[] a = {"0", "-6", "-0.5", "+2", "0,0(64)", "-0", "001", "0,(35)00", "-3,750"};

        String fractNonPeriod = "[.,]((\\d*[1-9]+)|([1-9]*0*[1-9]+))"; // дробная часть без периода
        String fractPeriod = "[.,](\\d*\\(0*[1-9]\\d*\\))"; // дробная часть с периодом
        String fract = "((" + fractNonPeriod + ")|(" + fractPeriod + "))"; // все виды возможных дробных частей

        String startZero = "[+-]?0" + fract; // веществ. число, начинающееся на +0, -0 или 0
        String startNonZero = "[+-]?[1-9]+0?\\d*" + fract + "?"; // любое вещественное число, начинающеся не с 0
        String zero = "0"; // просто число 0


        Pattern pattern = Pattern.compile("(" + startNonZero + ")|(" + startZero + ")|(" + zero + ")");

        StringBuilder sb1 = new StringBuilder("Подходят: ");
        StringBuilder sb2 = new StringBuilder("Не подходят: ");
        for (String s : a) {
            Matcher matcher = pattern.matcher(s);

            if (matcher.matches()) {
                sb1.append(s).append(";  ");
            } else {
                sb2.append(s).append(";  ");
            }
        }

        System.out.println(sb1);
        System.out.println(sb2);
    }
}
