/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.regex;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Генерировать случайные положительные целые числа c помощью java.util.Random. Вывести
первые 10 сгенерированных чисел, которые содержат более 3 и менее 6 четных цифр и ни
одной нечетной. Остановить генератор, вывести общее количество сгенерированных чисел.
Проверку осуществлять регулярным(-и) выражением(-ями). НЕ использовать
математические операции для анализа числа (сделать двумя способами – с помощью
matches и с помощью find).
 */

public class Task3 {
    public static void main(String[] args) {
        Random randomGenerator = new Random(99);

        int i = 0;
        while (i < 10) {
            int value = randomGenerator.nextInt(1000000);
            Pattern pattern1 = Pattern.compile("[0|2|4|6|8]{4}");
            Pattern pattern2 = Pattern.compile("[0|2|4|6|8]{5}");
            Matcher matcher1 = pattern1.matcher(value + "");
            Matcher matcher2 = pattern2.matcher(value + "");

            if ((value + "").length() == 4 && matcher1.find() ||
                    (value + "").length() == 5 && matcher2.find()) {
                System.out.println("Подходит: " + value);
                i++;
            }
        }

        /*int i = 0;
        while (i < 10) {
            int value = randomGenerator.nextInt(1000000);
            Pattern pattern = Pattern.compile("[0|2|4|6|8]{4,5}");
            Matcher matcher = pattern.matcher(value + "");

            if (matcher.matches()) {
                System.out.println("Подходит: " + value);
                i++;
            }
        }*/
    }
}