/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public class Flat implements Comparable<Flat> {
    int number;
    int length;
    int width;

    public Flat(int number, int length, int width) {
        this.number = number;
        this.length = length;
        this.width = width;
    }

    @Override
    public String toString() {
        return String.format("%d %d %d", number, length, width);
    }

    @Override
    public int compareTo(Flat f2) {
        return f2.length * f2.width - length * width;
    }
}
