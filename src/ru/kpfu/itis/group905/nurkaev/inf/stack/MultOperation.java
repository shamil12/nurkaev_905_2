/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public class MultOperation extends Operation {

    MultOperation() {
        this.priority = 2;
    }

    public boolean isPushNeeded(Stack<Operation> stack) {
        return !stack.isEmpty() && this.priority <= stack.peek().priority;
    }

    @Override
    public double value(double a, double b) {
        return a * b;
    }

    @Override
    public String value() {
        return "*";
    }

    @Override
    public String toString() {
        return "*";
    }
}
