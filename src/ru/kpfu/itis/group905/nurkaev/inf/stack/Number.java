/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public class Number implements StackElement {

    private final double value;

    public Number(double value) {
        this.value = value;
    }

    public String toString() {
        return String.valueOf(this.value);
    }

    @Override
    public double value(double a, double b) {
        return this.value;
    }

    @Override
    public String value() {
        return String.valueOf(this.value);
    }
}
