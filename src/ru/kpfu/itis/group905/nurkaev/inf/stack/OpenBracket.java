/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public class OpenBracket extends Operation {

    public OpenBracket() {
        this.priority = 0;
    }

    @Override
    public boolean isPushNeeded(Stack<Operation> stack) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double value(double a, double b) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String value() {
        return "(";
    }

    @Override
    public String toString() {
        return "(";
    }
}
