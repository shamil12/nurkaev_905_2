/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public abstract class Operation implements StackElement {

    public int priority;

    public Operation() {}

    public abstract String toString();

    public abstract boolean isPushNeeded(Stack<Operation> stack);

    @Override
    public abstract double value(double a, double b);

    @Override
    public abstract String value();
}
