/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public class PowOperation extends Operation {

    PowOperation() {
        this.priority = 3;
    }

    private static int pow(double a, int n) {
        int result = 1;
        while (n > 0) {
            if ((n & 1) == 1) {
                result *= a;
            }
            a *= a;
            n >>= 1;
        }
        return result;
    }

    public boolean isPushNeeded(Stack<Operation> stack) {
        return !stack.isEmpty() && this.priority <= stack.peek().priority;
    }

    @Override
    public double value(double a, double b) {
        return pow(b, (int) a);
    }

    @Override
    public String value() {
        return "^";
    }

    @Override
    public String toString() {
        return "^";
    }
}