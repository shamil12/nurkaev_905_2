/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

import java.util.LinkedList;
import java.util.Queue;

public class ReversePolishNotation {

    public static void main(String[] args) {
        LinkedList<String> expressions = new LinkedList<>();
        expressions.add("2 * 5 - 1"); // 9.0
        expressions.add("4 - 2 ^ 5 - 2"); // -30.0
        expressions.add("3 + 4 * 2 / ( 1 - 5 ) ^ 2"); // 3.5
        expressions.add("stop");
        count(expressions);
    }

    private static Queue<StackElement> reformat(String expression) {
        Stack<Operation> stack = new Stack<>();
        Queue<StackElement> RPN = new LinkedList<>(); // в RPN будет преобразовано арифметическое выражение
        String[] tokens = expression.split(" ");

        for (String token : tokens) {
            Operation operation = operation(token);
            if (isNumber(token)) {
                RPN.add(new Number(Double.parseDouble(token)));
            } else if (token.equals("(")) {
                stack.push(operation);
            } else if (token.equals(")")) {
                while (!stack.peek().value().equals("(")) {
                    RPN.add(stack.pop());
                }
                stack.pop(); // выкидываем открывающую скобку из стека
            } else if (operation != null) {
                while (operation.isPushNeeded(stack)) {
                    RPN.add(stack.pop());
                }
                stack.push(operation);
            }
        }

        while (!stack.isEmpty()) {
            RPN.add(stack.pop());
        }

        return RPN;
    }

    public static void count(LinkedList<String> expressions) {
        Stack<Double> stack = new Stack<>();

        int i = 0;
        Queue<StackElement> RPN;
        while (!expressions.get(i).equals("stop")) {
            RPN = reformat(expressions.get(i++));
            /*System.out.println(RPN);*/
            for (StackElement token : RPN) {
                Operation operation = operation(token.value());
                if (operation != null) {
                    stack.push(operation.value(stack.pop(), stack.pop()));
                } else {
                    stack.push(Double.parseDouble(token.value()));
                }
            }
            System.out.println(stack.pop());
        }
    }

    static Operation operation(String operation) {
        switch (operation) {
            case "+":
                return new SumOperation();
            case "-":
                return new SubOperation();
            case "*":
                return new MultOperation();
            case "/":
                return new DivOperation();
            case "^":
                return new PowOperation();
            case "(":
                return new OpenBracket();
            default:
                return null;
        }
    }

    private static boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}