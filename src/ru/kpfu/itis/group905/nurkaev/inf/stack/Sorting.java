/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Scanner;

public class Sorting {
    public static void main(String[] args) throws FileNotFoundException {
        String path = "src/ru/kpfu/itis/group905/nurkaev/inf/stack/in.txt";
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        Stack<Flat> flats = new Stack<>();

        while (scanner.hasNextLine()) {
            int number = scanner.nextInt();
            int length = scanner.nextInt();
            int width = scanner.nextInt();
            flats.push(new Flat(number, length, width));
        }
        scanner.close();

        System.out.println(flats);
        // а зачем вам компаратор?
        Collections.sort(flats);
        System.out.println(flats);

    }
}