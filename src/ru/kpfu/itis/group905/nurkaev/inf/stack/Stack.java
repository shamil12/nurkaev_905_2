/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

// package ДО ВСЕГО!
package ru.kpfu.itis.group905.nurkaev.inf.stack;

import java.util.*;

// ToDo: обратите внимание на желтый код, его в идеале быть не должно
public class Stack<T/* extends Comparable*/> implements List<T> {
    public Node<T> top;

    public int size;

    public void push(T val) {
        Node<T> curr = new Node<>(val);
        curr.next = top;
        top = curr;
        size += 1;
    }

    // ToDo: обратите внимание не пустые строки, они нужны чтобы разделить визуально логические блоки и облегчить понимание
    public T pop() {
        if (isEmpty()) {
            return null;
        }

        T val = top.val;
        top = top.next;
        size -= 1;
        return val;
    }

    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return top.val;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        top = null;
    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }

    @Override
    public Object[] toArray() {
        Object[] a = new Object[size];

        Node<T> curr = top;
        for (int i = 0; i < size; i++) {
            a[i] = curr.val;
            curr = curr.next;
        }

        return a;
    }

    // мы говорили, что так делать не надо
    @Override
    public ListIterator<T> listIterator() {
        return new ListIterator<T>() {

            Node<T> curr = top;
            int i = 0;

            @Override
            public boolean hasNext() {
                // CODE STYLE!
                if (i == 0) return true;

                boolean flag;
                try {
                    flag = curr.next != null;
                } catch (NullPointerException e) {
                    flag = false;
                }

                return flag;
            }

            @Override
            public T next() {
                if (i == 0) {
                    i++;
                    return curr.val;
                }

                T val;
                try {
                    curr = curr.next;
                    val = curr.val;
                    i++;
                } catch (NullPointerException e) {
                    throw new NoSuchElementException();
                }

                return val;
            }

            @Override
            public boolean hasPrevious() {
                throw new UnsupportedOperationException();
            }

            @Override
            public T previous() {
                throw new UnsupportedOperationException();
            }

            @Override
            public int nextIndex() {
                throw new UnsupportedOperationException();
            }

            @Override
            public int previousIndex() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void set(T t) {
                curr.val = t;
            }

            @Override
            public void add(T t) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        Node<T> curr = top;
        for (int i = 0; i < size; i++) {
            sb.append(curr.val).append("  ");
            curr = curr.next;
        }
        return sb.toString();
    }

    @Override
    public T get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException();
    }

//    public ListComparator<T> comparator() {
//        return new ListComparator<>();
//    }

    static class Node<T> {

        public T val;

        public Node<T> next;

        public Node(T val) {
            this.val = val;
        }
    }

    private static class ListComparator<L extends Comparable> implements Comparator<L> {
        @Override
        public int compare(L o1, L o2) {
            return o1.compareTo(o2);
        }
    }
}