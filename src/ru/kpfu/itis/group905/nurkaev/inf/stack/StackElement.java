/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.stack;

public interface StackElement {
    double value(double a, double b);

    String value();

    String toString();
}
