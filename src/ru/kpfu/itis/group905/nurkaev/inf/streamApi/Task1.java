/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Classwork
 */

package ru.kpfu.itis.group905.nurkaev.inf.streamApi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Task1 {

    private static final String ROOT_PATH =
            "src/ru/kpfu/itis/group905/nurkaev/inf/streamApi/";

    public static void main(String[] args) {

        // reading users.csv (id, name)
        Map<String, String> users = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(
                new FileReader(ROOT_PATH + "users.csv"))) {
            while (reader.ready()) {
                String[] line = reader.readLine().split(",");
                String id = line[0];
                String name = line[1];
                users.put(id, name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // reading subscriptions.csv
        Map<String, ArrayList<String>> subscriptions = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(
                new FileReader(ROOT_PATH + "subscriptions.csv"))) {
            while (reader.ready()) {
                String[] line = reader.readLine().split(",");
                String a_id = line[0];
                String b_id = line[1];
                subscriptions.putIfAbsent(a_id, new ArrayList<>());
                ArrayList<String> temp = subscriptions.get(a_id);
                temp.add(b_id);
                subscriptions.put(a_id, temp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // collecting data about friends
        Map<String, ArrayList<String>> friends = new HashMap<>();
        for (Map.Entry<String, String> entry : users.entrySet()) {
            String a_id = entry.getKey();

            // checking a_id has subscriptions
            if (!subscriptions.containsKey(a_id))
                continue;

            String a_name = entry.getValue();
            friends.putIfAbsent(a_name, new ArrayList<>());

            ArrayList<String> subs_of_a = subscriptions.get(a_id);
            for (String sub_of_a : subs_of_a)
                if (subscriptions.get(sub_of_a) != null &&
                        subscriptions.get(sub_of_a).contains(a_id)) {
                    ArrayList<String> temp = friends.get(a_name);
                    String b_name = users.get(sub_of_a);
                    temp.add(b_name);
                    friends.put(a_name, temp);
                }
        }
        System.out.println(friends);
    }
}