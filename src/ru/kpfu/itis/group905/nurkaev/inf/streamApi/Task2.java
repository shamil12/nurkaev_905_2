/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.streamApi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Task2 {

    private static final String ROOT_PATH =
            "src/ru/kpfu/itis/group905/nurkaev/inf/streamApi/";

    public static void main(String[] args) {

        // reading users.csv (id, city)
        Map<String, String> users = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(
                new FileReader(ROOT_PATH + "users.csv"))) {
            while (reader.ready()) {
                String[] line = reader.readLine().split(",");
                String id = line[0];
                String city = line[2];
                users.put(id, city);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(users);

        // reading subscriptions.csv (a_id : {cities})
        Map<String, ArrayList<String>> subscriptions = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(
                new FileReader(ROOT_PATH + "subscriptions.csv"))) {
            while (reader.ready()) {
                String[] line = reader.readLine().split(",");
                String a_id = line[0];
                String b_id = line[1];
                subscriptions.putIfAbsent(a_id, new ArrayList<>());
                ArrayList<String> temp = subscriptions.get(a_id);
                temp.add(users.get(b_id));
                subscriptions.put(a_id, temp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(subscriptions);

        // print those who subscribe
        // to people from the same city
        for (Map.Entry<String, ArrayList<String>> entry : subscriptions.entrySet()) {
            String a_id = entry.getKey();

            // checking a_id has subscriptions
            if (!subscriptions.containsKey(a_id))
                continue;

            ArrayList<String> cities = entry.getValue();
            String temp = cities.get(0);
            boolean sameCity = true;
            for (int i = 1; i < cities.size(); i++) {
                String city = cities.get(i);
                if (city.equals(temp))
                    temp = city;
                else {
                    sameCity = false;
                    break;
                }
            }

            if (sameCity)
                System.out.println(a_id);
        }
    }
}
