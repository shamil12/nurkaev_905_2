/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.strings;

// Класс String является final, что запрещает наследование от него

public class MyString implements Comparable<MyString> {

    private final String value;

    public MyString(String string) {
        this.value = string;
    }

    @Override
    public int compareTo(MyString o) {
        return value.compareTo(o.value);
    }

    @Override
    public String toString() {
        return value;
    }
}
