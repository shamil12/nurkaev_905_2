/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.strings;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        MyString[] a = {
                new MyString("Go"),
                new MyString("Back"),
                new MyString("To"),
                new MyString("Square"),
                new MyString("One")
        };

        Arrays.sort(a);
        for (MyString s : a) {
            System.out.println(s);
        }
    }
}
