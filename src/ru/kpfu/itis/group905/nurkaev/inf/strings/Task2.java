/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.strings;

import java.util.HashMap;

public class Task2 {
    public static void main(String[] args) {
        HashMap<Character, Integer> repetitions = count("Abra Kadabra");
        repetitions.forEach((k, v) -> System.out.println(k + " : " + v));
    }

    private static HashMap<Character, Integer> count(String s) {
        HashMap<Character, Integer> hm = new HashMap<>();

        s = s.toLowerCase();
        for (int i = 0; i < s.length(); i++) {
            hm.putIfAbsent(s.charAt(i), 0);
            hm.put(s.charAt(i), hm.get(s.charAt(i)) + 1);
        }

        return hm;
    }
}