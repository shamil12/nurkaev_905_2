/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.strings;

public class Task3 {
    public static void main(String[] args) {
        System.out.println(replace("moms and dads. KmomL", "mom", "dad"));
    }

    private static String replace(String s, String sub1, String sub2) {
        StringBuilder sb = new StringBuilder(s);

        for (int i = sub1.length() - 1; i < s.length(); i++) {
            if (sb.substring(i - 2, i + 1).equals(sub1)) {
                sb.replace(i - 2, i + 1, sub2);
            }
        }

        return sb.toString();
    }
}
