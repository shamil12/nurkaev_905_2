/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.strings;

public class Task4 {
    public static void main(String[] args) {
        System.out.println(joinOdd("Go back to square one"));
        System.out.println(joinOdd("Go back to square"));
        System.out.println(joinOdd("Go back to"));
        System.out.println(joinOdd("Go back"));
    }

    private static String joinOdd(String s) {
        StringBuilder result = new StringBuilder();

        int pos = 0;
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length(); i++) {
            if (pos % 2 == 0) {
                while (i < sb.length() && sb.charAt(i) != ' ') {
                    result.append(sb.charAt(i++));
                }
                result.append(' ');
            }
            if (i < sb.length() && sb.charAt(i) == ' ') pos++;
        }

        return result.toString();
    }
}