/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.tree;

import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        MyTree tree = new MyTree();
        int N = sc.nextInt();
        for (int i = 0; i < N; i++) tree.add(sc.nextInt());

        System.out.println("The sum of all maximums at each level of the tree: " + tree.sumMaximums_onEachLevel() + "\n");

        Map<Integer, Integer> map = tree.countsPstvNgtvZero(tree.root);
        System.out.println("Positive: " + map.get(1));
        System.out.println("Negative: " + map.get(-1));
        System.out.println("Zero: " + map.get(0));
    }
}
