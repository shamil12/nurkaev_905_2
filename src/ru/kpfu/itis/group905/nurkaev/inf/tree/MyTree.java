/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.tree;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyTree {
    public Node root;
    private Map<Integer, Integer> map = new LinkedHashMap<>();

    public MyTree() {
        map.put(1, 0);
        map.put(-1, 0);
        map.put(0, 0);
    }

    public void add(int val) {
        Node curr = new Node(val);
        if (root == null) root = curr;
        else {
            curr = root;
            while (true) {
                if (val < curr.val) {
                    if (curr.left == null) {
                        curr.left = new Node(val);
                        break;
                    }
                    curr = curr.left;
                } else {
                    if (curr.right == null) {
                        curr.right = new Node(val);
                        break;
                    }
                    curr = curr.right;
                }
            }
        }
    }

    public int sumMaximums_onEachLevel() {
        Node curr = root;
        int sumMaximums = 0;
        while (curr != null) {
            sumMaximums += curr.val;
            if (curr.right != null) curr = curr.right;
            else curr = curr.left;
        }
        return sumMaximums;
    }

    public Map<Integer, Integer> countsPstvNgtvZero(Node curr) {
        if (curr == null) return null;
        countsPstvNgtvZero(curr.left);
        countsPstvNgtvZero(curr.right);

        if (curr.val > 0)
            map.put(1, map.get(1) + 1);
        else if (curr.val < 0)
            map.put(-1, map.get(-1) + 1);
        else
            map.put(0, map.get(0) + 1);

        return map;
    }

    static class Node {
        int val;
        Node left;
        Node right;

        public Node(int val) {
            this.val = val;
        }
    }
}
