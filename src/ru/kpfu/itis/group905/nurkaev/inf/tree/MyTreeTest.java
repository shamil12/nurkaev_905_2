/**
 * @nshamil Shamil Nurkaev
 * 11-905
 * Homework
 */

package ru.kpfu.itis.group905.nurkaev.inf.tree;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class MyTreeTest {

    @Test
    public void positiveTest_countsPstvNgtvZero() {
        MyTree tree = new MyTree();
        int[] a = {1, 2, 0, -5, 2, -3};
        for (int i = 0; i < a.length; i++) tree.add(a[i]);
        Map<Integer, Integer> map = tree.countsPstvNgtvZero(tree.root);
        int positive = map.get(1);
        int negative = map.get(-1);
        int zero = map.get(0);
        Assert.assertEquals(3, positive);
        Assert.assertEquals(2, negative);
        Assert.assertEquals(1, zero);
    }

    @Test
    public void negativeTest_countsPstvNgtvZero() {
        MyTree tree = new MyTree();
        int[] a = {1, 4, -9, -16};
        for (int i = 0; i < a.length; i++) tree.add(a[i]);
        Map<Integer, Integer> map = tree.countsPstvNgtvZero(tree.root);
        int positive = map.get(1);
        int negative = map.get(-1);
        int zero = map.get(0);
        Assert.assertEquals(2, positive);
        Assert.assertEquals(2, negative);
        Assert.assertEquals(1, zero);
    }

    @Test
    public void positiveTest_sumMaximums_onEachLevel() {
        MyTree tree = new MyTree();
        int[] a = {100, 10, 6, 20, 15};
        for (int i = 0; i < a.length; i++) tree.add(a[i]);
        Assert.assertEquals(145, tree.sumMaximums_onEachLevel());
    }

    @Test
    public void negativeTest_sumMaximums_onEachLevel() {
        MyTree tree = new MyTree();
        int[] a = {2, 3, 1};
        for (int i = 0; i < a.length; i++) tree.add(a[i]);
        Assert.assertEquals(6, tree.sumMaximums_onEachLevel());
    }
}
